import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
    }),
  );
  // Configuracion de SWAGGER
  const config = new DocumentBuilder()
    .setTitle('Swagger Adm. Archivos Clinicos')
    .setDescription(
      'Sistema para Administración de Archivos Clínicos, para la Unidad de Archivos Clínicos',
    )
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api/v1/docs', app, document, {
    swaggerOptions: { filter: true },
  });
  // Fin
  // Habilitacion de CORS
  app.enableCors();
  // Fin
  await app.listen(3000);
  console.log('-----------------------------------------------');
  console.log('Sistema de Administración de Archivos Clinicos');
  console.log(`Servidor URL: ${process.env.SRV_HOST}`);
  console.log(`Servidor en escuha en el puerto: ${process.env.PORT}`);
  console.log('-----------------------------------------------');
}
bootstrap();
