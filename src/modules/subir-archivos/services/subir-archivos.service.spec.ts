import { Test, TestingModule } from '@nestjs/testing';
import { SubirArchivosService } from './subir-archivos.service';

describe('SubirArchivosService', () => {
  let service: SubirArchivosService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SubirArchivosService],
    }).compile();

    service = module.get<SubirArchivosService>(SubirArchivosService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
