import { Injectable } from '@nestjs/common';
import { ROUTE_ARGS_METADATA } from '@nestjs/common/constants';
import * as ftp from 'basic-ftp';
import { start } from 'repl';

@Injectable()
export class FtpService {
  private client: ftp.Client;

  constructor() {
    this.client = new ftp.Client();
    this.client.ftp.verbose = false;
  }

  /**
   * Conexion al Servidor FTP
   * @param host
   * @param port
   * @param user
   * @param password
   */
  async connect(
    host: string,
    port: number,
    user: string,
    password: string,
  ): Promise<void> {
    await this.client.access({
      host,
      port,
      user,
      password,
    });
  }

  async createStruct(ruta: string, carpeta: string): Promise<boolean> {
    const nuevaRuta = ruta.replaceAll('*', '/');
    await this.client.features();
    await this.client.cd(nuevaRuta);
    await this.client.ensureDir(`${carpeta}`);
    return true;
  }

  /**
   * Crea la estructura definida para el Afiliado
   * @param ruta
   * @param matricula
   * @param carpetas
   * @returns
   */
  async createFolders(
    ruta: string,
    matricula: string,
    tomo: string,
    carpetas: String,
  ): Promise<boolean> {
    // Verificacion de si existe la carpeta
    // Por no se crea la misma
    await this.client.ensureDir(ruta + '/' + matricula);
    // Crear Carpeta de Tomo
    await this.client.ensureDir(tomo);
    // Convertir a Array
    let car = carpetas.split(',');
    // Creación de sub carpetas
    for (let i = 0; i < car.length; i++) {
      await this.client.send('MKD ' + car[i]);
    }
    return true;
  }

  async createTomo(
    ruta: string,
    matricula: string,
    tomo: string,
    carpetas: string,
  ): Promise<boolean> {
    ruta = ruta + '*' + matricula;
    const nuevaRuta = ruta.replaceAll('*', '/');
    await this.client.cd(nuevaRuta);
    let car = carpetas.split(',');
    for (let i = 0; i < car.length; i++) {
      await this.client.send('MKD ' + car[i]);
    }
    return true;
  }

  // async createStruct(persona: string, gestion: number): Promise<boolean> {
  //   await this.client.ensureDir(`/${persona}/${gestion}/ENERO`);
  //   await this.client.ensureDir(`/${persona}/${gestion}/FEBRERO`);
  //   await this.client.ensureDir(`/${persona}/${gestion}/MARZO`);
  //   await this.client.ensureDir(`/${persona}/${gestion}/ABRIL`);
  //   await this.client.ensureDir(`/${persona}/${gestion}/MAYO`);
  //   await this.client.ensureDir(`/${persona}/${gestion}/JUNIO`);
  //   await this.client.ensureDir(`/${persona}/${gestion}/JULIO`);
  //   await this.client.ensureDir(`/${persona}/${gestion}/AGOSTO`);
  //   await this.client.ensureDir(`/${persona}/${gestion}/SEPTIEMBRE`);
  //   await this.client.ensureDir(`/${persona}/${gestion}/OCTUBRE`);
  //   await this.client.ensureDir(`/${persona}/${gestion}/NOVIEMBRE`);
  //   await this.client.ensureDir(`/${persona}/${gestion}/DICIEMBRE`);
  //   return true;
  // }

  /**
   * Cambiar el nombre de una Carpeta
   * @param ruta
   * @param newRuta
   * @returns
   */
  async renameFolder(ruta: string, newRuta: string) {
    ruta = '/' + ruta.replaceAll('*', '/');
    newRuta = '/' + newRuta.replaceAll('*', '/');
    return await this.client.rename(ruta, newRuta);
  }

  /**
   * Lista el contenido de la carpeta que se envia
   * @param ruta
   * @returns
   */
  async listFolder(ruta: string): Promise<any> {
    const nuevaRuta = ruta.replaceAll('*', '/');
    await this.client.cd(nuevaRuta);
    return await this.client.list();
  }

  async backFolder(ruta: string): Promise<any> {
    let r = ruta.split('*');
    r.splice(r.length - 1);
    let nuevaRuta = r.join('/');
    await this.client.cd(nuevaRuta);
    let list = await this.client.list();
    nuevaRuta = nuevaRuta.replaceAll('/', '*');
    return { lista: list, ruta: nuevaRuta };
  }

  async uploadFile(
    localPath: string,
    remotePath: string,
    fileName: string,
  ): Promise<any> {
    remotePath = remotePath.replaceAll('*', '/');
    return await this.client.uploadFrom(
      localPath,
      '/' + remotePath + '/' + fileName,
    );
  }

  async deleteFile(path: string, file: string, tipo: string) {
    let ruta = path + '*' + file;
    ruta = ruta.replaceAll('*', '/');
    return await this.client.remove('/' + ruta);

    // if (tipo == '1') {
    //   let ruta = path + '*' + file;
    //   ruta = ruta.replaceAll('*', '/');
    //   await this.client.remove('/' + ruta);
    // } else if (tipo == '2') {
    //   let ruta = path + '*' + file;
    //   ruta = ruta.replaceAll('*', '/');
    //   await this.client.removeDir('/' + ruta);
    // }
  }

  async deleteFolder(path: string, file: string, tipo: string) {
    let ruta = path + '*' + file;
    ruta = ruta.replaceAll('*', '/');
    // await this.client.removeDir('/ArchivosClinicos/90-5501 TIA/CARDENAS');
    return await this.client.removeDir('/' + ruta);
  }

  async downloadFile(pathFTP: string) {
    let partir = pathFTP.split('*');
    let ultimo = partir.pop();
    let ruta = partir.join('/');
    // pathDowload = pathDowload.replaceAll('*', '/');
    await this.client.cd(ruta);
    // return await this.client.downloadTo(pathDowload + '/' + ultimo, ultimo);
    return await this.client.downloadTo(
      'src/assets/download/' + ultimo,
      ultimo,
    );
  }

  /**
   * Cerrar la sesión actual
   */
  async disconnect(): Promise<void> {
    await this.client.close();
  }
}
