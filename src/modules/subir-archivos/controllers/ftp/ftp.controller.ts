import {
  Controller,
  Get,
  Param,
  Post,
  Res,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FtpService } from '../../services/ftp/ftp.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiTags } from '@nestjs/swagger';
import { Express } from 'express';
import { Observable, of } from 'rxjs';

@Controller('api/v1/ftp')
@ApiTags('API FTP')
export class FtpController {
  constructor(private readonly ftpService: FtpService) {}

  @Get('createStruct/:ruta/:carpeta')
  async createStruct(
    @Param('ruta') ruta: string,
    @Param('carpeta') carpeta: string,
  ): Promise<boolean> {
    // await this.ftpService.connect('192.168.1.111', 21, 'admin', 'admin');
    await this.ftpService.connect(
      process.env.FTP_HOST,
      parseInt(process.env.FTP_PORT),
      process.env.FTP_USER,
      process.env.FTP_PASS,
    );
    await this.ftpService.createStruct(ruta, carpeta);
    await this.ftpService.disconnect();
    return true;
  }

  @Get('createFolders/:ruta/:matricula/:tomo/:carpetas')
  async createFolders(
    @Param('ruta') ruta: string,
    @Param('matricula') matricula: string,
    @Param('tomo') tomo: string,
    @Param('carpetas') carpetas: String,
  ): Promise<boolean> {
    // await this.ftpService.connect('192.168.1.111', 21, 'admin', 'admin');
    await this.ftpService.connect(
      process.env.FTP_HOST,
      parseInt(process.env.FTP_PORT),
      process.env.FTP_USER,
      process.env.FTP_PASS,
    );
    await this.ftpService.createFolders(ruta, matricula, tomo, carpetas);
    await this.ftpService.disconnect();
    return true;
  }

  @Get('createTomo/:ruta/:matricula/:tomo/:carpetas')
  async createTomo(
    @Param('ruta') ruta: string,
    @Param('matricula') matricula: string,
    @Param('tomo') tomo: string,
    @Param('carpetas') carpetas: string,
  ): Promise<boolean> {
    await this.ftpService.connect(
      process.env.FTP_HOST,
      parseInt(process.env.FTP_PORT),
      process.env.FTP_USER,
      process.env.FTP_PASS,
    );
    await this.ftpService.createFolders(ruta, matricula, tomo, carpetas);
    await this.ftpService.disconnect();
    return true;
  }

  @Get('renameFolder/:ruta/:newRuta')
  async renameFolder(
    @Param('ruta') ruta: string,
    @Param('newRuta') newRuta: string,
  ): Promise<boolean> {
    await this.ftpService.connect(
      process.env.FTP_HOST,
      parseInt(process.env.FTP_PORT),
      process.env.FTP_USER,
      process.env.FTP_PASS,
    );
    let res: any = await this.ftpService.renameFolder(ruta, newRuta);
    await this.ftpService.disconnect();
    return res;
  }

  @Get('list/:ruta')
  async listStruct(@Param('ruta') ruta: string): Promise<any> {
    await this.ftpService.connect(
      process.env.FTP_HOST,
      parseInt(process.env.FTP_PORT),
      process.env.FTP_USER,
      process.env.FTP_PASS,
    );
    const list = await this.ftpService.listFolder(ruta);
    await this.ftpService.disconnect();
    return list;
  }

  @Get('back/:ruta')
  async backStruct(@Param('ruta') ruta: string): Promise<any> {
    await this.ftpService.connect(
      process.env.FTP_HOST,
      parseInt(process.env.FTP_PORT),
      process.env.FTP_USER,
      process.env.FTP_PASS,
    );
    const list = await this.ftpService.backFolder(ruta);
    await this.ftpService.disconnect();
    return list;
  }

  // @Get('list')
  // async listStruct(@Query() ruta: ftpDTO): Promise<any> {
  //   await this.ftpService.connect('192.168.1.111', 21, 'admin', 'admin');
  //   const list = await this.ftpService.listFolder(ruta.parentPath);
  //   await this.ftpService.disconnect();
  //   return list;
  // }

  @Post('upload/:ruta')
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(
    @UploadedFile() file: Express.Multer.File,
    @Param('ruta') ruta: string,
  ): Promise<any> {
    console.log('---------------------');
    console.log(file.path);
    console.log(ruta);
    console.log(file.originalname);
    console.log('---------------------');
    await this.ftpService.connect(
      process.env.FTP_HOST,
      parseInt(process.env.FTP_PORT),
      process.env.FTP_USER,
      process.env.FTP_PASS,
    );
    const up = await this.ftpService.uploadFile(
      file.path,
      ruta,
      file.originalname,
    );
    await this.ftpService.disconnect();
    return up;
  }

  @Get('delete/:ruta/:nombre/:tipo')
  async deleteFile(
    @Param('ruta') ruta: string,
    @Param('nombre') nombre: string,
    @Param('tipo') tipo: string,
  ): Promise<any> {
    let del: any;

    if (tipo == '1') {
      await this.ftpService.connect(
        process.env.FTP_HOST,
        parseInt(process.env.FTP_PORT),
        process.env.FTP_USER,
        process.env.FTP_PASS,
      );
      del = await this.ftpService.deleteFile(ruta, nombre, tipo);
      await this.ftpService.disconnect();
    } else if (tipo == '2') {
      await this.ftpService.connect(
        process.env.FTP_HOST,
        parseInt(process.env.FTP_PORT),
        process.env.FTP_USER,
        process.env.FTP_PASS,
      );
      del = await this.ftpService.deleteFolder(ruta, nombre, tipo);
      await this.ftpService.disconnect();
    }
    // const del = await this.ftpService.deleteFile(ruta, nombre, tipo);
    return del;
  }

  @Get('download/:rutaFtp')
  async downloadFile(@Param('rutaFtp') rutaFtp: string): Promise<any> {
    await this.ftpService.connect(
      process.env.FTP_HOST,
      parseInt(process.env.FTP_PORT),
      process.env.FTP_USER,
      process.env.FTP_PASS,
    );
    let down = await this.ftpService.downloadFile(rutaFtp);
    await this.ftpService.disconnect();
    return down;
  }

  @Get('archivo/:fileName')
  archivo(@Param('fileName') fileName: string, @Res() res): Observable<Object> {
    return of(
      res.sendFile(process.env.PATH_DOWNLOAD + fileName),
      // res.sendFile(
      //   'C:/Users/CHRISTIAN RENE/Documents/ProyectoAdmArchivosClinicos/Desarrollo/back-adm/src/assets/download/formulario.pdf',
      // ),
    );
  }
}
