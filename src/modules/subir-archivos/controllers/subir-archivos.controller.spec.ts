import { Test, TestingModule } from '@nestjs/testing';
import { SubirArchivosController } from './subir-archivos.controller';

describe('SubirArchivosController', () => {
  let controller: SubirArchivosController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SubirArchivosController],
    }).compile();

    controller = module.get<SubirArchivosController>(SubirArchivosController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
