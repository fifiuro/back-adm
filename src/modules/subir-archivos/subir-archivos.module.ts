import { Module } from '@nestjs/common';
import { SubirArchivosController } from './controllers/subir-archivos.controller';
import { SubirArchivosService } from './services/subir-archivos.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SubirArchivosEntity } from './entities/subir-archivos.entity';
import { FtpService } from './services/ftp/ftp.service';
import { FtpController } from './controllers/ftp/ftp.controller';
import { MulterModule } from '@nestjs/platform-express';

@Module({
  imports: [
    TypeOrmModule.forFeature([SubirArchivosEntity]),
    MulterModule.register({
      dest: './uploads',
    }),
  ],
  providers: [SubirArchivosService, FtpService],
  controllers: [SubirArchivosController, FtpController],
  exports: [SubirArchivosModule],
})
export class SubirArchivosModule {}
