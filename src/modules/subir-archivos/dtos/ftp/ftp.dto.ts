import { ApiProperty } from "@nestjs/swagger";
import { IsString } from "class-validator";

export class ftpDTO {
  @ApiProperty({example:'/'})
  @IsString({message:'La Ruta debe ser una cadena.'})
  parentPath: string;
}
