import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsBoolean,
  IsDate,
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import { CaratulaEntity } from '../entities/caratula.entity';
import { TiposAtencionEntity } from 'src/modules/tipo-atencion/entities/tipo-atencion.entity';
import { EspecialidadEntity } from 'src/modules/especialidad/entities/especialidad.entity';

export class CaratulaDTO {
  @ApiProperty({ example: '2023-08-28' })
  @IsNotEmpty()
  @IsDate()
  @Type(() => Date)
  fechaAtencion: Date;

  @ApiProperty({ example: '/820308CSC/2023/Consulta Extena/123456.pdf' })
  @IsNotEmpty()
  @IsString()
  ruta: string;

  @ApiProperty({ example: '20' })
  @IsNotEmpty()
  @IsNumber()
  fojas: number;

  @ApiProperty({ example: '2' })
  @IsNotEmpty()
  @IsNumber()
  cd: number;

  @ApiProperty({ example: '2' })
  @IsNotEmpty()
  @IsNumber()
  rx: number;

  @ApiProperty({ example: '2' })
  @IsNotEmpty()
  @IsNumber()
  tomografia: number;

  @ApiProperty({ example: 'A010 - FIEBRE TIFOIDEA' })
  @IsOptional()
  @IsString()
  cie: string;

  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  @IsNumber()
  tipoAtencion: TiposAtencionEntity;

  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  @IsNumber()
  especialidad: EspecialidadEntity;

  @ApiProperty({ example: true })
  @IsOptional()
  @IsBoolean()
  activo: boolean;
}

export class CaratulaUpdateDTO {
  @ApiProperty({ example: '2023-08-28' })
  @IsOptional()
  @IsDate()
  @Type(() => Date)
  fechaAtencion: Date;

  @ApiProperty({ example: '/820308CSC/2023/Consulta Extena/123456.pdf' })
  @IsOptional()
  @IsString()
  ruta: string;

  @ApiProperty({ example: '20' })
  @IsOptional()
  @IsNumber()
  fojas: number;

  @ApiProperty({ example: '2' })
  @IsOptional()
  @IsNumber()
  cd: number;

  @ApiProperty({ example: '2' })
  @IsOptional()
  @IsInt()
  rx: number;

  @ApiProperty({ example: '2' })
  @IsOptional()
  @IsNumber()
  tomografia: number;

  @ApiProperty({ example: 'A010 - FIEBRE TIFOIDEA' })
  @IsOptional()
  @IsString()
  cie: string;

  @ApiProperty({ example: '1' })
  @IsOptional()
  @IsNumber()
  tipoAtencion: TiposAtencionEntity;

  @ApiProperty({ example: '1' })
  @IsOptional()
  @IsNumber()
  especialidad: EspecialidadEntity;

  @ApiProperty({ example: true })
  @IsOptional()
  @IsBoolean()
  activo: boolean;
}

export class CaratulaListadoDTO {
  @IsNotEmpty()
  data: CaratulaDTO[];

  @IsNotEmpty()
  @IsNumber()
  count: number;

  @IsNotEmpty()
  @IsNumber()
  page: number;

  @IsNotEmpty()
  @IsNumber()
  pageSize: number;
}

export class ReporteDTO {
  @IsNumber()
  tipo: number;

  @IsNumber()
  total: number;
}

export class ParamReporteDTO {
  @IsNotEmpty()
  @IsNumber()
  tipoAtencion: number;

  @IsNotEmpty()
  @IsNumber()
  fIni: string;

  @IsNotEmpty()
  @IsNumber()
  fFin: string;
}
