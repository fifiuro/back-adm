import { BaseEntity } from 'src/core/shared/entities/base.entity';
import { EspecialidadEntity } from 'src/modules/especialidad/entities/especialidad.entity';
import { TiposAtencionEntity } from 'src/modules/tipo-atencion/entities/tipo-atencion.entity';
import { TomoEntity } from 'src/modules/tomos/entities/tomo.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity('caratulas')
export class CaratulaEntity extends BaseEntity {
  @PrimaryGeneratedColumn({ name: 'id_car' })
  idCar: number;

  @Column({
    type: 'date',
    nullable: false,
    name: 'fecha_atencion',
    unique: false,
  })
  fechaAtencion: Date;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'ruta',
    length: '500',
    unique: false,
  })
  ruta: string;

  @Column({
    type: 'int',
    nullable: false,
    name: 'fojas',
    unique: false,
  })
  fojas: number;

  @Column({
    type: 'int',
    nullable: false,
    name: 'cd',
    unique: false,
  })
  cd: number;

  @Column({
    type: 'int',
    nullable: false,
    name: 'rx',
    unique: false,
  })
  rx: number;

  @Column({
    type: 'int',
    nullable: false,
    name: 'tomografia',
    unique: false,
  })
  tomografia: number;

  @Column({
    type: 'varchar',
    nullable: true,
    name: 'cie',
    length:'500',
    unique: false,
  })
  cie: string;

  @ManyToOne(
    () => TiposAtencionEntity,
    (tipoAtencion: TiposAtencionEntity) => tipoAtencion.idTip,
  )
  tipoAtencion: TiposAtencionEntity;

  @ManyToOne(
    () => EspecialidadEntity,
    (especialidad: EspecialidadEntity) => especialidad.idEsp,
  )
  especialidad: EspecialidadEntity;

  @ManyToOne(() => TomoEntity, (tomo: TomoEntity) => tomo.idTom)
  tomo: TomoEntity;
}
