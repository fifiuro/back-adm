import { Module } from '@nestjs/common';
import { CaratulaService } from './services/caratula.service';
import { CaratulaController } from './controllers/caratula.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CaratulaEntity } from './entities/caratula.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CaratulaEntity])],
  controllers: [CaratulaController],
  providers: [CaratulaService],
  exports: [CaratulasModule, CaratulaService],
})
export class CaratulasModule {}
