import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CaratulaEntity } from '../entities/caratula.entity';
import {
  Between,
  ILike,
  Repository,
  UpdateResult,
  getConnection,
} from 'typeorm';
import {
  CaratulaDTO,
  CaratulaListadoDTO,
  CaratulaUpdateDTO,
  ReporteDTO,
} from '../dtos/caratula.dto';
import { ErrorManager } from 'src/core/shared/errorManager/error.manager';
import { PaginationQueryDto } from 'src/core/shared/dtos';

@Injectable()
export class CaratulaService {
  constructor(
    @InjectRepository(CaratulaEntity)
    private readonly caratulaRepository: Repository<CaratulaEntity>,
  ) {}

  public async createCaratula(body: CaratulaDTO): Promise<CaratulaEntity> {
    try {
      const caratula = await this.caratulaRepository.save(body);
      if (caratula) {
        return caratula;
      } else {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: 'No se Resgitro Caratula.',
        });
      }
    } catch (error) {
      // throw new Error(error);
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findCaratula({
    limit,
    offset,
  }: PaginationQueryDto): Promise<CaratulaListadoDTO> {
    try {
      const take: number = limit || 10;
      const skip: number = (offset - 1) * take;
      const [result, total] = await this.caratulaRepository.findAndCount({
        order: { idCar: 'ASC' },
        take: take,
        skip: skip,
        relations: ['tipoAtencion', 'especialidad', 'tomo'],
      });
      if (result.length === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: 'No se encontro resultados, de Caratulas.',
        });
      }
      return { data: result, count: total, page: offset, pageSize: limit };
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findCaratulaById(idCar: number): Promise<CaratulaEntity> {
    try {
      if (idCar > 0) {
        const caratula: CaratulaEntity = await this.caratulaRepository.findOne({
          where: { idCar: idCar },
          relations: ['tipoAtencion', 'especialidad', 'tomo'],
        });
        if (caratula) {
          return caratula;
        } else {
          throw new ErrorManager({
            type: 'BAD_REQUEST',
            message: `No encontramos Caratula, con el ID: ${idCar}`,
          });
        }
      } else {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `El ID debe ser un número y no este valor: ${idCar}.`,
        });
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findCaratulaByRuta(ruta: string): Promise<CaratulaEntity> {
    try {
      if (ruta) {
        const caratula: CaratulaEntity = await this.caratulaRepository.findOne({
          where: { ruta: ruta },
        });
        if (caratula) {
          return caratula;
        } else {
          throw new ErrorManager({
            type: 'BAD_REQUEST',
            message: `No encontramos Caratula, con la ruta: ${ruta}`,
          });
        }
      } else {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `La Ruta debe ser una cadena y debe tener un valor: ${ruta}`,
        });
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findCaratulaByRutaLike(ruta: string): Promise<number> {
    try {
      if (ruta) {
        const caratula: number = await this.caratulaRepository.count({
          where: { ruta: ILike(`${ruta}%`), activo: true },
        });
        if (caratula) {
          return caratula;
        } else {
          return caratula;
          // throw new ErrorManager({
          //   type:'BAD_REQUEST',
          //   message:`No encontramos Caratulas, con la ruta:${ruta}`
          // })
        }
      } else {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `La Ruta debe ser una cadena y debe tener un valor: ${ruta}`,
        });
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findPersonaCaratulaFecha(
    id: number,
    tipoAtencion: number,
    especialidad: number,
    fInicio: string,
    fFin: string,
  ): Promise<CaratulaEntity[]> {
    let combinacion: string = '';
    if (id) {
      combinacion += '1';
    } else {
      combinacion += '0';
    }
    if (tipoAtencion) {
      combinacion += '1';
    } else {
      combinacion += '0';
    }
    if (especialidad) {
      combinacion += '1';
    } else {
      combinacion += '0';
    }
    if (fInicio != '0' && fFin != '0') {
      combinacion += '1';
    } else {
      combinacion += '0';
    }
    try {
      const fi = new Date(fInicio);
      const ff = new Date(fFin);
      if (combinacion == '1100') {
        const caratula: CaratulaEntity[] = await this.caratulaRepository.find({
          where: {
            tomo: { persona: { idPer: id } },
            tipoAtencion: { idTip: tipoAtencion },
          },
          relations: ['tipoAtencion', 'especialidad', 'tomo'],
        });
        if (caratula) {
          return caratula;
        } else {
          throw new ErrorManager({
            type: 'BAD_REQUEST',
            message: `No encontramos Caratula, con los datos proporcionados.`,
          });
        }
      } else if (combinacion == '1010') {
        const caratula: CaratulaEntity[] = await this.caratulaRepository.find({
          where: {
            tomo: { persona: { idPer: id } },
            especialidad: { idEsp: especialidad },
          },
          relations: ['tipoAtencion', 'especialidad', 'tomo'],
        });
        if (caratula) {
          return caratula;
        } else {
          throw new ErrorManager({
            type: 'BAD_REQUEST',
            message: `No encontramos Caratula, con los datos proporcionados.`,
          });
        }
      } else if (combinacion == '1001') {
        const fi = new Date(fInicio);
        const ff = new Date(fFin);
        const caratula: CaratulaEntity[] = await this.caratulaRepository.find({
          where: {
            tomo: { persona: { idPer: id } },
            fechaAtencion: Between(fi, ff),
          },
          relations: ['tipoAtencion', 'especialidad', 'tomo'],
        });
        if (caratula) {
          return caratula;
        } else {
          throw new ErrorManager({
            type: 'BAD_REQUEST',
            message: `No encontramos Caratula, con los datos proporcionados.`,
          });
        }
      } else if (combinacion == '1110') {
        const caratula: CaratulaEntity[] = await this.caratulaRepository.find({
          where: {
            tomo: { persona: { idPer: id } },
            tipoAtencion: { idTip: tipoAtencion },
            especialidad: { idEsp: especialidad },
          },
          relations: ['tipoAtencion', 'especialidad', 'tomo'],
        });
        if (caratula) {
          return caratula;
        } else {
          throw new ErrorManager({
            type: 'BAD_REQUEST',
            message: `No encontramos Caratula, con los datos proporcionados.`,
          });
        }
      } else if (combinacion == '1101') {
        const fi = new Date(fInicio);
        const ff = new Date(fFin);
        const caratula: CaratulaEntity[] = await this.caratulaRepository.find({
          where: {
            tomo: { persona: { idPer: id } },
            tipoAtencion: { idTip: tipoAtencion },
            fechaAtencion: Between(fi, ff),
          },
          relations: ['tipoAtencion', 'especialidad', 'tomo'],
        });
        if (caratula) {
          return caratula;
        } else {
          throw new ErrorManager({
            type: 'BAD_REQUEST',
            message: `No encontramos Caratula, con los datos proporcionados.`,
          });
        }
      } else if (combinacion == '1011') {
        const fi = new Date(fInicio);
        const ff = new Date(fFin);
        const caratula: CaratulaEntity[] = await this.caratulaRepository.find({
          where: {
            tomo: { persona: { idPer: id } },
            especialidad: { idEsp: especialidad },
            fechaAtencion: Between(fi, ff),
          },
          relations: ['tipoAtencion', 'especialidad', 'tomo'],
        });
        if (caratula) {
          return caratula;
        } else {
          throw new ErrorManager({
            type: 'BAD_REQUEST',
            message: `No encontramos Caratula, con los datos proporcionados.`,
          });
        }
      } else if (combinacion == '1111') {
        const fi = new Date(fInicio);
        const ff = new Date(fFin);
        const caratula: CaratulaEntity[] = await this.caratulaRepository.find({
          where: {
            tipoAtencion: { idTip: tipoAtencion },
            especialidad: { idEsp: especialidad },
            tomo: { persona: { idPer: id } },
            fechaAtencion: Between(fi, ff),
          },
          relations: ['tipoAtencion', 'especialidad', 'tomo'],
        });
        if (caratula) {
          return caratula;
        } else {
          throw new ErrorManager({
            type: 'BAD_REQUEST',
            message: `No encontramos Caratula, con los datos proporcionados.`,
          });
        }
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async reportLoadDate(fInicio: string, fFin: string): Promise<any> {
    try {
      if (fInicio && fFin) {
        const reporte: any = await this.caratulaRepository
          .createQueryBuilder('caratulas')
          .select('caratulas.tipoAtencion as tipo, COUNT(*) as total')
          .where(
            `caratulas.createdAt between Date('${fInicio}') and Date('${fFin}')`,
          )
          .groupBy('caratulas.tipoAtencion')
          .orderBy('caratulas.tipoAtencion')
          .getRawMany();
        if (reporte.length > 0) {
          console.log(reporte);
          return reporte;
        } else {
          throw new ErrorManager({
            type: 'BAD_REQUEST',
            message: `No encontramos lo que estas buscando.`,
          });
        }
      } else {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: 'Debe mandar valores.',
        });
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async updadeCaratula(
    body: CaratulaUpdateDTO,
    idCar: number,
  ): Promise<UpdateResult | undefined> {
    try {
      const caratula: UpdateResult = await this.caratulaRepository.update(
        idCar,
        body,
      );
      if (caratula.affected === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `No se encontro Caratula, con el ID: ${idCar}. Para la modificación.`,
        });
      }
      return caratula;
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async eliminarCaratulas(
    body: CaratulaUpdateDTO,
    ruta: string,
  ): Promise<UpdateResult | undefined> {
    try {
      const car: CaratulaEntity = await this.caratulaRepository.findOne({
        where: { ruta: ruta },
      });

      const caratula: UpdateResult = await this.caratulaRepository.update(
        car.idCar,
        body,
      );
      if (caratula.affected == 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: 'No se pudo realizar la tarea de Actualizar campo activo.',
        });
      }
      return caratula;
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }
}
