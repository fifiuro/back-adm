import { Test, TestingModule } from '@nestjs/testing';
import { CaratulaService } from './caratula.service';

describe('CaratulaService', () => {
  let service: CaratulaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CaratulaService],
    }).compile();

    service = module.get<CaratulaService>(CaratulaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
