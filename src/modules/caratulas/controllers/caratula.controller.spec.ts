import { Test, TestingModule } from '@nestjs/testing';
import { CaratulaController } from './caratula.controller';

describe('CaratulaController', () => {
  let controller: CaratulaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CaratulaController],
    }).compile();

    controller = module.get<CaratulaController>(CaratulaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
