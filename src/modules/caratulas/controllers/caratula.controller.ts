import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { CaratulaService } from '../services/caratula.service';
import { CaratulaDTO, CaratulaUpdateDTO } from '../dtos/caratula.dto';
import { PaginationQueryDto } from 'src/core/shared/dtos';
import { AuthGuard } from 'src/core/guards/auth.guards';

@Controller('api/v1/caratula')
@ApiTags('API Caratula')
@UseGuards(AuthGuard)
export class CaratulaController {
  constructor(private readonly caratulaService: CaratulaService) {}

  @Post('add')
  @ApiOperation({ description: 'Crea un nuevo registro de Caratula.' })
  public async addCaratula(@Body() body: CaratulaDTO) {
    return await this.caratulaService.createCaratula(body);
  }

  @Get('all')
  @ApiOperation({ description: 'Lista todos las Caratulas.' })
  public async allCaratula(@Query() pagination: PaginationQueryDto) {
    return await this.caratulaService.findCaratula(pagination);
  }

  @Get('id/:id')
  @ApiOperation({
    description: 'Devuelve la Caratula segun el ID enviado.',
  })
  public async idCaratula(@Param('id') id: number) {
    return await this.caratulaService.findCaratulaById(id);
  }

  @Get('ruta/:ruta')
  @ApiOperation({ description: 'Devuelve la Caratula segun la Ruta enviada.' })
  public async rutaCaratula(@Param('ruta') ruta: string) {
    return await this.caratulaService.findCaratulaByRuta(ruta);
  }

  @Get('rutaLike/:ruta')
  @ApiOperation({
    description: 'Devuelve el numero archivos en Caratula segun Ruta enviada.',
  })
  public async findCaratulaByRutaLike(@Param('ruta') ruta: string) {
    return await this.caratulaService.findCaratulaByRutaLike(ruta);
  }

  @Get('buscar/:id/:tipoAtencion/:especialidad/:fInicio/:fFin')
  @ApiOperation({
    description:
      'Hace la busqueda segun matricula, tipo atencion y especialidad',
  })
  public async buscarCaratula(
    @Param('id') id: number,
    @Param('tipoAtencion') tipoAtencion: number,
    @Param('especialidad') especialidad: number,
    @Param('fInicio') fInicio: string,
    @Param('fFin') fFin: string,
  ) {
    return await this.caratulaService.findPersonaCaratulaFecha(
      id,
      tipoAtencion,
      especialidad,
      fInicio,
      fFin,
    );
  }

  @Get('reporteLoadDate/:tipoAtencion/:fInicio/:fFin')
  @ApiOperation({
    description:
      'Reporte del numero de subidas de archivos segun Tipo de Atención y rango de Fechas.',
  })
  public async buscarLoadDate(
    @Param('tipoAtencion') tipoAtencion: number,
    @Param('fInicio') fInicio: string,
    @Param('fFin') fFin: string,
  ) {
    return await this.caratulaService.reportLoadDate(fInicio, fFin);
  }

  @Put('update/:id')
  @ApiOperation({ description: 'Actualiza los datos de Caratula.' })
  public async updateUnidad(
    @Param('id') id: number,
    @Body() body: CaratulaUpdateDTO,
  ) {
    return await this.caratulaService.updadeCaratula(body, id);
  }

  @Put('eliminar/:ruta')
  @ApiOperation({ description: 'Actualiza el campo activo del archivo.' })
  public async eliminarCaratula(
    @Param('ruta') ruta: string,
    @Body() body: CaratulaUpdateDTO,
  ) {
    return await this.caratulaService.eliminarCaratulas(body, ruta);
  }
}
