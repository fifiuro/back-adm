import { Controller, Get, Param, Res, UseGuards } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ReportesService } from '../services/reportes.service';
import { AuthGuard } from 'src/core/guards/auth.guards';

import { Response } from 'express';
import { Observable, of } from 'rxjs';

@Controller('api/v1/reportes')
@ApiTags('API Reportes')
@UseGuards(AuthGuard)
export class ReportesController {
  constructor(private readonly reportesService: ReportesService) {}

  @Get('tipo/:fInicio/:fFin')
  @ApiOperation({
    description: 'Listado de los archivos subidos segun rango de Fechas.',
  })
  public async reporteTipo(
    @Param('fInicio') fInicio: string,
    @Param('fFin') fFin: string,
  ) {
    return await this.reportesService.countCaratulas(fInicio, fFin);
  }

  @Get('download/:fInicio/:fFin')
  @ApiOperation({ description: 'Descarga el reporte en formato PDF.' })
  public async downloadReporteTipo(
    @Param('fInicio') fInicio: string,
    @Param('fFin') fFin: string,
    @Res() res: Response,
  ): Promise<Observable<Object>> {
    const fs = require('fs');
    const carbone = require('carbone');
    const data = await this.reportesService.countCaratulas(fInicio, fFin);
    let options = {
      convertTo: 'pdf',
      reportName: 'reporteConteo.pdf',
    };
    carbone.render(
      './src/assets/plantillas/reporteConteo.odt',
      data,
      options,
      (err: any, buffer: any) => {
        if (err) {
          return console.log(err);
        }
        res.send(Buffer.from(buffer));
        return of(res);
      },
    );
    return;
  }
}
