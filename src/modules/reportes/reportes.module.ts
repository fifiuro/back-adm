import { Module } from '@nestjs/common';
import { ReportesService } from './services/reportes.service';
import { ReportesController } from './controller/reportes.controller';
import { CaratulaService } from '../caratulas/services/caratula.service';
import { CaratulasModule } from '../caratulas/caratulas.module';
import { TipoAtencionService } from '../tipo-atencion/services/tipo-atencion.service';
import { TipoAtencionModule } from '../tipo-atencion/tipo-atencion.module';

@Module({
  imports: [CaratulasModule, TipoAtencionModule],
  providers: [ReportesService],
  controllers: [ReportesController],
})
export class ReportesModule {}
