import { Injectable } from '@nestjs/common';
import { ReportesDTO } from '../dtos/reportes.dto';
import { CaratulaService } from 'src/modules/caratulas/services/caratula.service';
import { TipoAtencionService } from 'src/modules/tipo-atencion/services/tipo-atencion.service';
import { ErrorManager } from 'src/core/shared/errorManager/error.manager';

import { Response } from 'express';

@Injectable()
export class ReportesService {
  constructor(
    private readonly caratulaService: CaratulaService,
    private readonly tipoAtencionService: TipoAtencionService,
  ) {}

  public async countCaratulas(
    fInicio: string,
    fFin: string,
  ): Promise<ReportesDTO[]> {
    try {
      const reporte: ReportesDTO[] = await this.caratulaService.reportLoadDate(
        fInicio,
        fFin,
      );
      const tipo = await this.tipoAtencionService.findTipoAtencionCombo();
      if (reporte && tipo) {
        reporte.map(async (e: any) => {
          tipo.map((t) => {
            if (t.idTip == e.tipo) {
              e.tipo = t.tipo;
            }
          });
        });
        return reporte;
      } else {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: 'No se encontramos lo que esta buscando.',
        });
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }
}
