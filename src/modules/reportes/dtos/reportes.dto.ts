import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString } from 'class-validator';

export class ReportesDTO {
  @ApiProperty({ example: 1 })
  @IsOptional()
  @IsNumber()
  tipoAtencion: number;

  @ApiProperty({ example: '2023-10-03' })
  @IsOptional()
  @IsString()
  fIni: string;

  @ApiProperty({ example: '2023-11-03' })
  @IsOptional()
  @IsString()
  fFin: string;
}
