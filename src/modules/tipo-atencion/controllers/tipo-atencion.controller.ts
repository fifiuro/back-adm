import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { TipoAtencionService } from '../services/tipo-atencion.service';
import {
  TipoAtencionDTO,
  TipoAtencionUpdateDTO,
} from '../dtos/tipo-atencion.dto';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { PaginationQueryDto } from 'src/core/shared/dtos/pagination-query.dto';
import { AuthGuard } from 'src/core/guards/auth.guards';

@Controller('api/v1/tipo-atencion')
@ApiTags('API Tipo de Atención')
@UseGuards(AuthGuard)
export class TipoAtencionController {
  constructor(private readonly tipoAtencionService: TipoAtencionService) {}
  @Post('add')
  @ApiOperation({ description: 'Crea un nuevo registro de Tipo de Atención.' })
  public async addTipoAtencion(@Body() body: TipoAtencionDTO) {
    return await this.tipoAtencionService.createTipoAtencion(body);
  }

  @Get('all')
  @ApiOperation({ description: 'Lista todos los Tipo de Atención.' })
  public async allTipoAtencion(@Query() pagination: PaginationQueryDto) {
    return await this.tipoAtencionService.findTipoAtencion(pagination);
  }

  @Get('like/:texto')
  @ApiOperation({
    description:
      'Lista todas las coincidencias con el texto enviado a Tipo de Atención.',
  })
  public async likeTipoAtencion(
    @Param('texto') texto: string,
    @Query() pagination: PaginationQueryDto,
  ) {
    return await this.tipoAtencionService.findTipoAtencionLike(
      pagination,
      texto,
    );
  }

  @Get('combo')
  @ApiOperation({
    description:
      'Lista todos los Tipo de Atención pero con estado activo igual TRUE.',
  })
  public async comboTipoAtencion() {
    return await this.tipoAtencionService.findTipoAtencionCombo();
  }

  @Get('id/:id')
  @ApiOperation({
    description: 'Devuelve el Tipo de Atención segun el ID enviado.',
  })
  public async idTipoAtencion(@Param('id') id: number) {
    return await this.tipoAtencionService.findTipoAtencionById(id);
  }

  @Put('update/:id')
  @ApiOperation({ description: 'Actualiza los datos de Tipo de Atención.' })
  public async updateTipoAtencion(
    @Param('id') id: number,
    @Body() body: TipoAtencionUpdateDTO,
  ) {
    return await this.tipoAtencionService.updadeTipoAtencion(body, id);
  }
}
