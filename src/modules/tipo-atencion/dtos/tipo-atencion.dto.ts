import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class TipoAtencionDTO {
  @ApiProperty({ example: 'Consulta Externa' })
  @IsNotEmpty({ message: 'El Tipo de Atención no debe estar vacío.' })
  @IsString({ message: 'El Tipo de Atención debe ser de tipo cadena.' })
  tipo: string;

  @ApiProperty({ example: true })
  @IsOptional()
  @IsBoolean({ message: 'La propiedad de Activo debe ser: Verdadero o Falso.' })
  activo: boolean;
}

export class TipoAtencionUpdateDTO {
  @ApiProperty({ example: 'Consulta Externa' })
  @IsOptional()
  @IsString({ message: 'El Tipo de Atención debe ser de una cadena.' })
  tipo: string;

  @ApiProperty({ example: true })
  @IsOptional()
  @IsBoolean({ message: 'La propiedad de Activo debe ser: Verdadero o Falso.' })
  activo: boolean;
}

export class TipoAtencionListadoDTO {
  @IsNotEmpty()
  data: TipoAtencionDTO[];

  @IsNotEmpty({ message: 'Conteo no debe ser vacío.' })
  @IsNumber()
  count: number;

  @IsNotEmpty({ message: 'Página no debe ser vacío.' })
  @IsNumber()
  page: number;

  @IsNotEmpty({ message: 'Número de elementos por Página no debe ser vacío.' })
  @IsNumber()
  pageSize: number;
}
