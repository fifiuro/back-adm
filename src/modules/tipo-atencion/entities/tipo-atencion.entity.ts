import { BaseEntity } from 'src/core/shared/entities/base.entity';
import { CaratulaEntity } from 'src/modules/caratulas/entities/caratula.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity('tipos_atencion')
export class TiposAtencionEntity extends BaseEntity {
  @PrimaryGeneratedColumn({ name: 'id_tip' })
  idTip: number;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'tipo',
    length: '255',
    unique: false,
  })
  tipo: string;

  @OneToMany(() => CaratulaEntity, (caratula: CaratulaEntity) => caratula.idCar)
  caratula: CaratulaEntity[];
}
