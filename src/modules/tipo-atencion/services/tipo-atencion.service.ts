import { Injectable } from '@nestjs/common';
import { TiposAtencionEntity } from '../entities/tipo-atencion.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { ILike, Repository, UpdateResult } from 'typeorm';
import {
  TipoAtencionDTO,
  TipoAtencionListadoDTO,
  TipoAtencionUpdateDTO,
} from '../dtos/tipo-atencion.dto';
import { PaginationQueryDto } from 'src/core/shared/dtos/pagination-query.dto';
import { ErrorManager } from 'src/core/shared/errorManager/error.manager';

@Injectable()
export class TipoAtencionService {
  constructor(
    @InjectRepository(TiposAtencionEntity)
    private readonly tipoAtencionRepository: Repository<TiposAtencionEntity>,
  ) {}

  public async createTipoAtencion(
    body: TipoAtencionDTO,
  ): Promise<TiposAtencionEntity> {
    try {
      const duplicado = await this.checkIfTipoAtencionExists({
        key: 'tipo',
        value: body.tipo,
      });
      if (duplicado) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `Ya existe el registro de Tipo de Atención. con el valor: ${body.tipo}`,
        });
      } else {
        return await this.tipoAtencionRepository.save(body);
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  private async checkIfTipoAtencionExists({
    key,
    value,
  }: {
    key: keyof TipoAtencionDTO;
    value: any;
  }): Promise<boolean> {
    const tipoAtencion: TiposAtencionEntity[] =
      await this.tipoAtencionRepository.find({
        where: {
          [key]: value,
        },
      });
    if (tipoAtencion.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  public async findTipoAtencion({
    limit,
    offset,
  }: PaginationQueryDto): Promise<TipoAtencionListadoDTO> {
    try {
      const take: number = limit || 10;
      const skip: number = (offset - 1) * take;
      const [result, total] = await this.tipoAtencionRepository.findAndCount({
        order: { activo: 'DESC' },
        take: take,
        skip: skip,
      });
      if (result.length === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: 'No se encontro resultados, de Tipo de Atención.',
        });
      }
      return { data: result, count: total, page: offset, pageSize: limit };
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findTipoAtencionLike(
    { limit, offset }: PaginationQueryDto,
    texto: string,
  ): Promise<TipoAtencionListadoDTO> {
    try {
      const take = limit || 10;
      const skip = (offset - 1) * take;
      const [result, total] = await this.tipoAtencionRepository.findAndCount({
        where: {
          tipo: ILike(`%${texto}%`),
        },
        order: { idTip: 'ASC' },
        take: take,
        skip: skip,
      });
      if (result.length === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `No se encontro resultados, con: ${texto}`,
        });
      }
      return { data: result, count: total, page: offset, pageSize: limit };
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findTipoAtencionCombo(): Promise<TiposAtencionEntity[]> {
    try {
      const tipoAtencion: TiposAtencionEntity[] =
        await this.tipoAtencionRepository.find({ where: { activo: true } });
      if (tipoAtencion.length === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: 'No se encontro resultado Tipo de Atención.',
        });
      }
      return tipoAtencion;
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findTipoAtencionById(
    idTip: number,
  ): Promise<TiposAtencionEntity> {
    try {
      if (idTip > 0) {
        const tipoAtencion: TiposAtencionEntity =
          await this.tipoAtencionRepository.findOne({
            where: { idTip: idTip },
          });
        if (tipoAtencion) {
          return tipoAtencion;
        } else {
          throw new ErrorManager({
            type: 'BAD_REQUEST',
            message: `No encontramos Tipo de Atención, con el ID: ${idTip}`,
          });
        }
      } else {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `El ID debe ser un número y no este valor: ${idTip}.`,
        });
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async updadeTipoAtencion(
    body: TipoAtencionUpdateDTO,
    idTip: number,
  ): Promise<UpdateResult | undefined> {
    try {
      if (idTip) {
        const tipoAtencion: UpdateResult =
          await this.tipoAtencionRepository.update(idTip, body);
        if (tipoAtencion.affected === 0) {
          throw new ErrorManager({
            type: 'BAD_REQUEST',
            message: `No se encontro Tipo de Atención, con el ID: ${idTip}. Para la modificación.`,
          });
        }
        return tipoAtencion;
      } else {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `El ID no tiene ningun valor.`,
        });
      }
    } catch (error) {
      throw new Error(error);
      throw ErrorManager.createSignatureError(error.message);
    }
  }
}
