import { Module } from '@nestjs/common';
import { TipoAtencionController } from './controllers/tipo-atencion.controller';
import { TipoAtencionService } from './services/tipo-atencion.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TiposAtencionEntity } from './entities/tipo-atencion.entity';

@Module({
  imports: [TypeOrmModule.forFeature([TiposAtencionEntity])],
  controllers: [TipoAtencionController],
  providers: [TipoAtencionService],
  exports: [TypeOrmModule, TipoAtencionService],
})
export class TipoAtencionModule {}
