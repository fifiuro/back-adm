import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { MedicoService } from '../services/medico.service';
import { MedicoDTO } from '../dtos/medico.dto';
import { PaginationQueryDto } from 'src/core/shared/dtos/pagination-query.dto';
import { AuthGuard } from 'src/core/guards/auth.guards';

@Controller('api/v1/medico')
@ApiTags('API Medico')
@UseGuards(AuthGuard)
export class MedicoController {
  constructor(private readonly medicoService: MedicoService) {}

  @Post('add')
  @ApiOperation({ description: 'Crea un nuevo registro de Medico.' })
  public async addMedico(@Body() body: MedicoDTO) {
    return await this.medicoService.createMedico(body);
  }

  @Get('all')
  @ApiOperation({ description: 'Lista todos los Medicos.' })
  public async allMedico(@Query() pagination: PaginationQueryDto) {
    return await this.medicoService.findMedico(pagination);
  }

  @Get('like/:texto')
  @ApiOperation({
    description: 'Lista todas las coincidencias con el texto enviado a Medico.',
  })
  public async likeMedico(
    @Param('texto') texto: string,
    @Query() pagination: PaginationQueryDto,
  ) {
    return await this.medicoService.findMedicoLike(pagination, texto);
  }

  @Get('verifica/:nombre/:paterno/:materno/:especialidad')
  @ApiOperation({ description: 'Veririfica que el Medico existe.' })
  public async checkExistMedico(
    @Param('nombre') nombre: string,
    @Param('paterno') paterno: string,
    @Param('materno') materno: string,
    @Param('especialidad') especialidad: string,
  ) {
    return await this.medicoService.checkExistMedico(
      nombre,
      paterno,
      materno,
      especialidad,
    );
  }

  @Get('combo')
  @ApiOperation({
    description: 'Lista todos los Medicos pero con estado activo igual TRUE.',
  })
  public async comboMedico() {
    return await this.medicoService.findMedicoCombo();
  }

  @Get('id/:id')
  @ApiOperation({
    description: 'Devuelve el Meidco segun el ID enviado.',
  })
  public async idMedico(@Param('id') id: number) {
    return await this.medicoService.findMedicoById(id);
  }

  @Put('update/:id')
  @ApiOperation({ description: 'Actualiza los datos de Medico.' })
  public async updateMedico(@Param('id') id: number, @Body() body: MedicoDTO) {
    return await this.medicoService.updadeMedico(body, id);
  }
}
