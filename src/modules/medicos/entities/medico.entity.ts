import { BaseEntity } from 'src/core/shared/entities/base.entity';
import { PrestamoEntity } from 'src/modules/prestamos/entities/prestamo.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity('medicos')
export class MedicoEntity extends BaseEntity {
  @PrimaryGeneratedColumn({ name: 'id_med' })
  idMed: number;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'nombres',
    length: '150',
    unique: false,
  })
  nombres: string;
  @Column({
    type: 'varchar',
    nullable: false,
    name: 'ap_paterno',
    length: '150',
    unique: false,
  })
  apPaterno: string;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'ap_materno',
    length: '150',
    unique: false,
  })
  apMaterno: string;

  @Column({
    type: 'varchar',
    nullable: true,
    name: 'nombre_completo',
    length: 200,
    unique: false,
  })
  nombreCompleto: string;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'especialidad',
    length: '150',
    unique: false,
  })
  especialidad: string;

  @OneToMany(() => PrestamoEntity, (prestamo: PrestamoEntity) => prestamo.idPre)
  prestamo: PrestamoEntity[];
}
