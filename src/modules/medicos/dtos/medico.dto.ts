import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class MedicoDTO {
  @ApiProperty({ example: 'Lucio' })
  @IsNotEmpty()
  @IsString()
  nombres: string;

  @ApiProperty({ example: 'Moya' })
  @IsNotEmpty()
  @IsString()
  apPaterno: string;

  @ApiProperty({ example: 'Moya' })
  @IsNotEmpty()
  @IsString()
  apMaterno: string;

  @ApiProperty({ example: 'Lucio Moya Moya' })
  @IsNotEmpty()
  @IsString()
  nombreCompleto: string;

  @ApiProperty({ example: 'Terapia Intensiva' })
  @IsNotEmpty()
  @IsString()
  especialidad: string;

  @ApiProperty({ example: true })
  @IsOptional()
  @IsBoolean()
  activo: boolean;
}

export class MedicoUpdateDTO {
  @ApiProperty({ example: 'Lucio' })
  @IsOptional()
  @IsString()
  nombres: string;

  @ApiProperty({ example: 'Moya' })
  @IsOptional()
  @IsString()
  apPaterno: string;

  @ApiProperty({ example: 'Moya' })
  @IsOptional()
  @IsString()
  apMaterno: string;

  @ApiProperty({ example: 'Lucio Moya Moya' })
  @IsOptional()
  @IsString()
  nombreCompleto: string;

  @ApiProperty({ example: 'Terapia Intensiva' })
  @IsOptional()
  @IsString()
  especialidad: string;

  @ApiProperty({ example: true })
  @IsOptional()
  @IsBoolean()
  activo: boolean;
}

export class MedicoListadoDTO {
  @IsNotEmpty()
  data: MedicoDTO[];

  @IsNotEmpty()
  @IsNumber()
  count: number;

  @IsNotEmpty()
  @IsNumber()
  page: number;

  @IsNotEmpty()
  @IsNumber()
  pageSize: number;
}
