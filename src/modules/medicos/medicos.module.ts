import { Module } from '@nestjs/common';
import { MedicoService } from './services/medico.service';
import { MedicoController } from './controllers/medico.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MedicoEntity } from './entities/medico.entity';

@Module({
  imports: [TypeOrmModule.forFeature([MedicoEntity])],
  controllers: [MedicoController],
  providers: [MedicoService],
  exports: [TypeOrmModule],
})
export class MedicosModule {}
