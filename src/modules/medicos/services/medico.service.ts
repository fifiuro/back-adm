import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MedicoEntity } from '../entities/medico.entity';
import { ILike, Repository, UpdateResult } from 'typeorm';
import {
  MedicoDTO,
  MedicoListadoDTO,
  MedicoUpdateDTO,
} from '../dtos/medico.dto';
import { ErrorManager } from 'src/core/shared/errorManager/error.manager';
import { PaginationQueryDto } from 'src/core/shared/dtos';

@Injectable()
export class MedicoService {
  constructor(
    @InjectRepository(MedicoEntity)
    private readonly medicoRepository: Repository<MedicoEntity>,
  ) {}

  public async createMedico(body: MedicoDTO): Promise<MedicoEntity> {
    try {
      const duplicado = await this.checkIfMedicoExists({
        key: 'nombres',
        value: body.nombres,
      });
      if (duplicado) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `Ya existe el registro del Medico. con el valor: ${body.nombres}`,
        });
      } else {
        return await this.medicoRepository.save(body);
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  private async checkIfMedicoExists({
    key,
    value,
  }: {
    key: keyof MedicoDTO;
    value: string;
  }): Promise<boolean> {
    const medico: MedicoEntity[] = await this.medicoRepository.find({
      where: {
        [key]: value,
      },
    });
    if (medico.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  public async findMedico({
    limit,
    offset,
  }: PaginationQueryDto): Promise<MedicoListadoDTO> {
    try {
      const take: number = limit || 10;
      const skip: number = (offset - 1) * take;
      const [result, total] = await this.medicoRepository.findAndCount({
        order: { idMed: 'ASC' },
        take: take,
        skip: skip,
      });
      if (result.length === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: 'No se encontro resultados, del Medico.',
        });
      }
      return { data: result, count: total, page: offset, pageSize: limit };
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findMedicoLike(
    { limit, offset }: PaginationQueryDto,
    texto: string,
  ): Promise<MedicoListadoDTO> {
    try {
      const take = limit || 10;
      const skip = (offset - 1) * take;
      const [result, total] = await this.medicoRepository.findAndCount({
        where: {
          nombres: ILike(`%${texto}%`),
        },
        order: { idMed: 'ASC' },
        take: take,
        skip: skip,
      });
      if (result.length === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `No se encontro resultados, con: ${texto}`,
        });
      }
      return { data: result, count: total, page: offset, pageSize: limit };
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async checkExistMedico(
    nombre: string,
    paterno: string,
    materno: string,
    especialidad: string,
  ): Promise<MedicoEntity[]> {
    try {
      const medico: MedicoEntity[] = await this.medicoRepository.find({
        where: {
          nombres: nombre,
          apPaterno: paterno,
          apMaterno: materno,
          especialidad: especialidad,
        },
      });
      if (medico.length === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: 'No se encontro resultado de Medico.',
        });
      }
      return medico;
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findMedicoCombo(): Promise<MedicoEntity[]> {
    try {
      const medico: MedicoEntity[] = await this.medicoRepository.find({
        where: { activo: true },
      });
      if (medico.length === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: 'No se encontro resultado de Medico.',
        });
      }
      return medico;
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findMedicoById(idMed: number): Promise<MedicoEntity> {
    try {
      if (idMed > 0) {
        const medico: MedicoEntity = await this.medicoRepository.findOne({
          where: { idMed: idMed },
        });
        if (medico) {
          return medico;
        } else {
          throw new ErrorManager({
            type: 'BAD_REQUEST',
            message: `No encontramos Persona, con el ID: ${idMed}`,
          });
        }
      } else {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `El ID debe ser un número y no este valor: ${idMed}.`,
        });
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async updadeMedico(
    body: MedicoUpdateDTO,
    idMed: number,
  ): Promise<UpdateResult | undefined> {
    try {
      const medico: UpdateResult = await this.medicoRepository.update(
        idMed,
        body,
      );
      if (medico.affected === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `No se encontro Medico, con el ID: ${idMed}. Para la modificación.`,
        });
      }
      return medico;
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }
}
