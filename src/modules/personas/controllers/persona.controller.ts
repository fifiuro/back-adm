import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { PersonaService } from '../services/persona.service';
import { PersonaDTO } from '../dtos/persona.dto';
import { PaginationQueryDto } from 'src/core/shared/dtos/pagination-query.dto';
import { AuthGuard } from 'src/core/guards/auth.guards';

@Controller('api/v1/persona')
@ApiTags('API Personas')
@UseGuards(AuthGuard)
export class PersonaController {
  constructor(private readonly personaService: PersonaService) {}

  @Post('add')
  @ApiOperation({ description: 'Crea un nuevo registro de Persona.' })
  public async addPersona(@Body() body: PersonaDTO) {
    return await this.personaService.createPersona(body);
  }

  @Get('all')
  @ApiOperation({ description: 'Lista todos las Personas.' })
  public async allPersona(@Query() pagination: PaginationQueryDto) {
    return await this.personaService.findPersona(pagination);
  }

  @Get('like/:texto')
  @ApiOperation({
    description:
      'Lista todas las coincidencias con el texto enviado a Personas.',
  })
  public async likePersona(
    @Param('texto') texto: string,
    @Query() pagination: PaginationQueryDto,
  ) {
    return await this.personaService.findPersonaLike(pagination, texto);
  }

  @Get('combo')
  @ApiOperation({
    description: 'Lista todos las Personas pero con estado activo igual TRUE.',
  })
  public async comboPersona() {
    return await this.personaService.findPersonaCombo();
  }

  @Get('id/:id')
  @ApiOperation({
    description: 'Devuelve la Persona segun el ID enviado.',
  })
  public async idPersona(@Param('id') id: number) {
    return await this.personaService.findPersonaById(id);
  }

  @Put('update/:id')
  @ApiOperation({ description: 'Actualiza los datos de Persona.' })
  public async updatePersona(
    @Param('id') id: number,
    @Body() body: PersonaDTO,
  ) {
    return await this.personaService.updatePersona(body, id);
  }
}
