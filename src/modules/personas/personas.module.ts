import { Module } from '@nestjs/common';
import { PersonaService } from './services/persona.service';
import { PersonaController } from './controllers/persona.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PersonasEntity } from './entities/persona.entity';

@Module({
  imports: [TypeOrmModule.forFeature([PersonasEntity])],
  controllers: [PersonaController],
  providers: [PersonaService, PersonaService],
  exports: [PersonasModule, PersonaService],
})
export class PersonasModule {}
