import { BaseEntity } from 'src/core/shared/entities/base.entity';
import { PersonaUnidadEntity } from 'src/modules/persona-unidad/entities/persona-unidad.entity';
import { PrestamoEntity } from 'src/modules/prestamos/entities/prestamo.entity';
import { TomoEntity } from 'src/modules/tomos/entities/tomo.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity('personas')
export class PersonasEntity extends BaseEntity {
  @PrimaryGeneratedColumn({ name: 'id_per' })
  idPer: number;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'nombres',
    length: '255',
    unique: false,
  })
  nombres: string;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'ap_paterno',
    length: '255',
    unique: false,
  })
  apPaterno: string;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'ap_materno',
    length: '255',
    unique: false,
  })
  apMaterno: string;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'nombre_completo',
    length: '255',
    unique: false,
  })
  nombreCompleto: string;
  
  @Column({
    type: 'varchar',
    nullable: false,
    name: 'matricula_titular',
    length: '50',
    unique: false,
  })
  matriculaTitular: string;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'matricula_beneficiario',
    length: '50',
    unique: false,
  })
  matriculaBeneficiario: string;

  @Column({
    type: 'varchar',
    nullable: true,
    name: 'ci_titular',
    length: '20',
    unique: false,
  })
  ciTitular: string;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'ci_beneficiario',
    length: '20',
    unique: false,
  })
  ciBeneficiario: string;

  @Column({
    type: 'varchar',
    nullable: true,
    name: 'complemento',
    length: '20',
    unique: false,
  })
  complemento: string;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'expedido',
    length: '50',
    unique: false,
  })
  expedido: string;

  @Column({
    type: 'date',
    nullable: false,
    name: 'fecha_nac',
    unique: false,
  })
  fechaNac: Date;

  @Column({
    type: 'int',
    nullable: false,
    name: 'edad',
    unique: false,
  })
  edad: number;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'sexo',
    length: '45',
    unique: false,
  })
  sexo: string;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'cod_patronal',
    length: '50',
    unique: false,
  })
  codPatronal: string;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'empresa',
    length: '500',
    unique: false,
  })
  empresa: string;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'estado_afiliacion',
    length: '50',
    unique: false,
  })
  estadoAfiliacion: string;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'tipo_afiliacion',
    length: '50',
    unique: false,
  })
  tipoAfiliacion: string;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'codigo_descripcion',
    length: '50',
    unique: false,
  })
  codigoDescripcion: string;

  @OneToMany(() => TomoEntity, (tomo: TomoEntity) => tomo.idTom)
  tomo: TomoEntity[];

  @OneToMany(() => PrestamoEntity, (prestamo: PrestamoEntity) => prestamo.idPre)
  prestamo: PrestamoEntity[];

  @OneToMany(
    () => PersonaUnidadEntity,
    (personaUnidad: PersonaUnidadEntity) => personaUnidad.idPu,
  )
  personaUnidad: PersonaUnidadEntity[];
}
