import { Injectable, Module } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PersonasEntity } from '../entities/persona.entity';
import { ILike, Repository, UpdateResult } from 'typeorm';
import {
  PersonaDTO,
  PersonaListadoDTO,
  PersonaUpdateDTO,
} from '../dtos/persona.dto';
import { ErrorManager } from 'src/core/shared/errorManager/error.manager';
import { PaginationQueryDto } from 'src/core/shared/dtos/pagination-query.dto';
import { promises } from 'dns';

@Injectable()
export class PersonaService {
  constructor(
    @InjectRepository(PersonasEntity)
    private readonly personaRepository: Repository<PersonasEntity>,
  ) {}

  public async createPersona(body: PersonaDTO): Promise<PersonasEntity> {
    try {
      const duplicado = await this.checkIfPersonaExists({
        key: 'matriculaTitular',
        value: body.matriculaTitular,
      });
      if (duplicado) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `Ya existe el registro de Persona. con el valor: ${body.matriculaTitular}`,
        });
      } else {
        return await this.personaRepository.save(body);
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  private async checkIfPersonaExists({
    key,
    value,
  }: {
    key: keyof PersonaDTO;
    value: any;
  }): Promise<boolean> {
    console.log(key);
    console.log(value);
    const persona: PersonasEntity[] = await this.personaRepository.find({
      where: {
        matriculaTitular: value,
      },
    });
    console.log('Verificacion de Duplicado de PERSONA...');
    console.log(persona);
    if (persona.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  public async findPersona({
    limit,
    offset,
  }: PaginationQueryDto): Promise<PersonaListadoDTO> {
    try {
      const take: number = limit || 10;
      const skip: number = (offset - 1) * take;
      const [result, total] = await this.personaRepository.findAndCount({
        order: { idPer: 'ASC' },
        take: take,
        skip: skip,
      });
      if (result.length === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: 'No se encontro resultados, de Personas.',
        });
      }
      return { data: result, count: total, page: offset, pageSize: limit };
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findPersonaLike(
    { limit, offset }: PaginationQueryDto,
    texto: string,
  ): Promise<PersonaListadoDTO> {
    try {
      const take = limit || 10;
      const skip = (offset - 1) * take;
      const [result, total] = await this.personaRepository.findAndCount({
        where: {
          nombres: ILike(`%${texto}%`),
        },
        order: { idPer: 'ASC' },
        take: take,
        skip: skip,
      });
      if (result.length === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `No se encontro resultados, con: ${texto}`,
        });
      }
      return { data: result, count: total, page: offset, pageSize: limit };
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findPersonaCombo(): Promise<PersonasEntity[]> {
    try {
      const persona: PersonasEntity[] = await this.personaRepository.find({
        where: { activo: true },
      });
      if (persona.length === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: 'No se encontro resultado de Personas.',
        });
      }
      return persona;
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findPersonaById(idTip: number): Promise<PersonasEntity> {
    try {
      if (idTip > 0) {
        const persona: PersonasEntity = await this.personaRepository.findOne({
          where: { idPer: idTip },
        });
        if (persona) {
          return persona;
        } else {
          throw new ErrorManager({
            type: 'BAD_REQUEST',
            message: `No encontramos Personas, con el ID: ${idTip}`,
          });
        }
      } else {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `El ID debe ser un número y no este valor: ${idTip}.`,
        });
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async updatePersona(
    body: PersonaUpdateDTO,
    idTip: number,
  ): Promise<UpdateResult | undefined> {
    try {
      const persona: UpdateResult = await this.personaRepository.update(
        idTip,
        body,
      );
      if (persona.affected === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `No se encontro Persona, con el ID: ${idTip}. Para la modificación.`,
        });
      }
      return persona;
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }
}
