import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsBoolean,
  IsDate,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class PersonaDTO {
  @ApiProperty({ example: 'Luis' })
  @IsNotEmpty({ message: 'El Nombre no debe estar vacio.' })
  @IsString({ message: 'El Nombre debe ser un String.' })
  nombres: string;

  @ApiProperty({ example: 'Mamani' })
  @IsNotEmpty({ message: 'El Apellido Paterno no debe estar vacio.' })
  @IsString({ message: 'El Apellido Paterno debe ser un String.' })
  apPaterno: string;

  @ApiProperty({ example: 'Mamani' })
  @IsNotEmpty({ message: 'El Apellido Materno no debe estar vacio.' })
  @IsString({ message: 'El Apellido Materno debe ser un String.' })
  apMaterno: string;

  @ApiProperty({ example: 'Luis Mamani Mamani' })
  @IsNotEmpty({ message: 'El Nombre Completo no debe ser vacio.' })
  @IsString({ message: 'El Nombre Completo debe ser un String.' })
  nombreCompleto: string;

  @ApiProperty({ example: '800513MML' })
  @IsOptional()
  @IsString({ message: 'El Matricula Titular debe ser un String.' })
  matriculaTitular: string;

  @ApiProperty({ example: '' })
  @IsOptional()
  @IsString({ message: 'El Matricula del Beneficiario debe ser un String.' })
  matriculaBeneficiario: string;

  @ApiProperty({ example: '48961235' })
  @IsOptional()
  @IsString({
    message: 'El Carnet de Identidad del Titular debe ser un String.',
  })
  ciTitular: string;

  @ApiProperty({ example: '' })
  @IsOptional()
  @IsString({
    message: 'El Carnet de Identidad del Beneficiario debe ser un String.',
  })
  ciBeneficiario: string;

  @ApiProperty({ example: '' })
  @IsOptional()
  @IsString({ message: 'El Complemento debe ser un String.' })
  complemento: string;

  @ApiProperty({ example: 'LA PAZ' })
  @IsNotEmpty({ message: 'El dato Expedido no debe estar vacio.' })
  @IsString({ message: 'El dato Expedido debe ser un String.' })
  expedido: string;

  @ApiProperty({ example: '1990-11-10' })
  @IsNotEmpty({ message: 'La Fecha de Nacimiento no debe estar vacio.' })
  @IsDate({ message: 'La Fecha de Nacimiento debe ser de tipo Fecha.' })
  @Type(() => Date)
  fechaNac: Date;

  @ApiProperty({ example: '32' })
  @IsNotEmpty({ message: 'La Edad no debe estar vacio.' })
  @IsNumber()
  edad: number;

  @ApiProperty({ example: 'Masculino' })
  @IsNotEmpty({ message: 'El dato de Sexo no debe estar vacio.' })
  @IsString({ message: 'El dato de Sexo debe ser un String.' })
  sexo: string;

  @ApiProperty({ example: '123456EMP' })
  @IsNotEmpty({ message: 'El Código Patronal no debe estar vacio.' })
  @IsString({ message: 'El Código Patronal debe ser un String.' })
  codPatronal: string;

  @ApiProperty({ example: 'SAN LUIS S.R.L.' })
  @IsNotEmpty({ message: 'El Nombre de la Empresa no debe estar vacio.' })
  @IsString({ message: 'El Nombre de la Empresa debe ser un String.' })
  empresa: string;

  @ApiProperty({ example: 'ACTIVO' })
  @IsNotEmpty({ message: 'El Estado de Afiliación no debe estar vacio.' })
  @IsString({ message: 'El Estado de Aficliación debe ser un String.' })
  estadoAfiliacion: string;

  @ApiProperty({ example: 'ACTIVO' })
  @IsNotEmpty({ message: 'El Tipo de Aficliación no debe estar vacio.' })
  @IsString({ message: 'El Tipo de Aficliación debe ser un String.' })
  tipoAfiliacion: string;

  @ApiProperty({ example: 'TITULAR' })
  @IsNotEmpty({ message: 'La Descripción de Código no debe estar vacio.' })
  @IsString({ message: 'La Descripción de Código debe ser un String.' })
  codigoDescripcion: string;

  @ApiProperty({ example: true })
  @IsOptional()
  @IsBoolean({ message: 'El datao de Activo debe ser Verdadero o Falso.' })
  activo: boolean;
}

export class PersonaUpdateDTO {
  @ApiProperty({ example: 'Luis' })
  @IsOptional()
  @IsString({ message: 'El Nombre debe ser un String.' })
  nombres: string;

  @ApiProperty({ example: 'Mamani' })
  @IsOptional()
  @IsString({ message: 'El Apellido Paterno debe ser un String.' })
  apPaterno: string;

  @ApiProperty({ example: 'Mamani' })
  @IsOptional()
  @IsString({ message: 'El Apellido Materno debe ser un String.' })
  apMaterno: string;

  @ApiProperty({ example: 'Luis Mamani Mamani' })
  @IsOptional()
  @IsString({ message: 'El Nombre Completo debe ser un String.' })
  nombreCompleto: string;

  @ApiProperty({ example: '800513MML' })
  @IsOptional()
  @IsString({ message: 'El Matricula Titular debe ser un String.' })
  matriculaTitular: string;

  @ApiProperty({ example: '' })
  @IsOptional()
  @IsString({ message: 'El Matricula del Beneficiario debe ser un String.' })
  matriculaBeneficiario: string;

  @ApiProperty({ example: '48961235' })
  @IsOptional()
  @IsString({
    message: 'El Carnet de Identidad del Titular debe ser un String.',
  })
  ciTitular: string;

  @ApiProperty({ example: 'uis' })
  @IsOptional()
  @IsString({
    message: 'El Carnet de Identidad del Beneficiario debe ser un String.',
  })
  ciBeneficiario: string;

  @ApiProperty({ example: '' })
  @IsOptional()
  @IsString({ message: 'El Complemento debe ser un String.' })
  complemento: string;

  @ApiProperty({ example: 'LA PAZ' })
  @IsOptional()
  @IsString({ message: 'El Expedido debe ser un String.' })
  expedido: string;

  @ApiProperty({ example: '1990-11-10' })
  @IsOptional()
  @IsDate({ message: 'La Fecha de Nacimiento debe ser de tipo Fecha.' })
  @Type(() => Date)
  fechaNac: Date;

  @ApiProperty({ example: '32' })
  @IsOptional()
  @IsNumber()
  edad: number;

  @ApiProperty({ example: 'Masculino' })
  @IsOptional()
  @IsString({ message: 'El dato de Sexo debe ser un String.' })
  sexo: string;

  @ApiProperty({ example: '123456EMP' })
  @IsOptional()
  @IsString({ message: 'El Código Patronal debe ser un String.' })
  codPatronal: string;

  @ApiProperty({ example: 'SAN LUIS S.R.L.' })
  @IsOptional()
  @IsString({ message: 'El Nombre de la Empresa debe ser un String.' })
  empresa: string;

  @ApiProperty({ example: 'ACTIVO' })
  @IsOptional()
  @IsString({ message: 'El Estado de Aficliación debe ser un String.' })
  estadoAfiliacion: string;

  @ApiProperty({ example: 'ACTIVO' })
  @IsOptional()
  @IsString({ message: 'El Tipo de Aficliación debe ser un String.' })
  tipoAfiliacion: string;

  @ApiProperty({ example: 'TITULOR' })
  @IsOptional()
  @IsString({ message: 'La Descripción de Código debe ser un String.' })
  codigoDescripcion: string;

  @ApiProperty({ example: true })
  @IsOptional()
  @IsBoolean({ message: 'El datao de Activo debe ser Verdadero o Falso.' })
  activo: boolean;
}

export class PersonaListadoDTO {
  @IsNotEmpty()
  data: PersonaDTO[];

  @IsNotEmpty()
  @IsNumber()
  count: number;

  @IsNotEmpty()
  @IsNumber()
  page: number;

  @IsNotEmpty()
  @IsNumber()
  pageSize: number;
}
