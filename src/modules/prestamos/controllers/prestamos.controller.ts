import { Body, Controller, Get, Param, Post, Put, Query } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { PrestamosService } from '../services/prestamos.service';
import { PrestamoDTO, PrestamoUpdateDTO } from '../dtos/prestamo.dto';
import { PaginationQueryDto } from 'src/core/shared/dtos';
import { PaginationBuscarDto } from 'src/core/shared/dtos/pagination-query.dto';

@Controller('api/v1/prestamos')
@ApiTags('API Prestamo')
export class PrestamosController {
  constructor(private readonly prestamoService: PrestamosService) {}

  @Post('add')
  @ApiOperation({ description: 'Crea un nuevo registro de Prestamo.' })
  public async addPrestamo(@Body() body: PrestamoDTO) {
    return await this.prestamoService.createPrestamo(body);
  }

  @Get('all')
  @ApiOperation({ description: 'Lista todos los Prestamos.' })
  public async allPrestamo(@Query() pagination: PaginationQueryDto) {
    return await this.prestamoService.findPrestamo(pagination);
  }

  @Get('id/:id')
  @ApiOperation({
    description: 'Devuelve el Prestamo segun el ID enviado.',
  })
  public async idPrestamo(@Param('id') id: number) {
    return await this.prestamoService.findPrestamoById(id);
  }

  @Get('prestamox3')
  @ApiOperation({
    description:
      'Buscar Prestamos segun el nombre del Afiliado, nombre de Medico y estado del Prestamo',
  })
  public async x3Prestamo(@Query() pagination: PaginationBuscarDto) {
    return await this.prestamoService.findPrestamoX3(pagination);
  }

  @Put('update/:id')
  @ApiOperation({ description: 'Actualiza los datos de Prestamo.' })
  public async updatePrestamo(
    @Param('id') id: number,
    @Body() body: PrestamoUpdateDTO,
  ) {
    return await this.prestamoService.updadePrestamo(body, id);
  }

  @Get('fecha')
  @ApiOperation({ description: 'Retorna la fecha actual del Servidor.' })
  public async fecha() {
    return await this.prestamoService.fecha();
  }
}
