import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsBoolean,
  IsDate,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import { MedicoEntity } from 'src/modules/medicos/entities/medico.entity';
import { PersonasEntity } from 'src/modules/personas/entities/persona.entity';
import { TomoEntity } from 'src/modules/tomos/entities/tomo.entity';

export class PrestamoDTO {
  @ApiProperty({ example: '2013-03-15' })
  @IsNotEmpty({ message: 'Fecha de Prestamo no debe estar vacio.' })
  @IsString({ message: 'Fecha de Prestamo debe ser una Fecha Valida.' })
  fechaPrestamo: string;

  @ApiProperty({ example: '15:30' })
  @IsNotEmpty({ message: 'Hora de Prestamo no debe estar vacio.' })
  @IsString({ message: 'Hora de Prestamo debe ser una Hora Valida.' })
  horaPrestamo: string;

  @ApiProperty({ example: 'Esto es una prueba para las observaciones.' })
  @IsNotEmpty({ message: 'Observaciones no debe esta vavio.' })
  @IsString({ message: 'Observaciones debe ser cadena.' })
  obsPrestamo: string;

  @ApiProperty({ example: '2013-03-15' })
  @IsOptional()
  @IsString({ message: 'Fecha de Devolución debe ser una Fecha Valida.' })
  fechaDevolucion: string;

  @ApiProperty({ example: '15:30' })
  @IsOptional()
  @IsString({ message: 'Hora de Devolución debe ser una Hoa Valida.' })
  horaDevolucion: string;

  @ApiProperty({ example: 'Esto es una prueba para las observaciones.' })
  @IsOptional()
  @IsString({ message: 'Observaciones debe ser una cadena.' })
  obsDevolucion: string;

  @ApiProperty({ example: 'PRESTAMO' })
  @IsNotEmpty({ message: 'Estado no debe estar vacio.' })
  @IsString({
    message: 'Estado debe ser una cadena, como: PRESTAMO o DEVUELTO',
  })
  estado: string;

  @ApiProperty({ example: true })
  @IsOptional()
  @IsBoolean({
    message: 'Activo debe ser de tipo Booleano, por ejemplo TRUE o False.',
  })
  activo: boolean;

  @ApiProperty({ example: 1 })
  @IsNotEmpty({ message: 'Debe seleccionar una Persona o Afiliado' })
  @IsNumber()
  persona: PersonasEntity;

  @ApiProperty({ example: 1 })
  @IsNotEmpty({ message: 'Debe seleccionar un Tomo de la lista.' })
  @IsNumber()
  tomo: TomoEntity;

  @ApiProperty({ example: 1 })
  @IsNotEmpty({ message: 'Debe seleccionar un Medico de la lista.' })
  @IsNumber()
  medico: MedicoEntity;
}

export class PrestamoUpdateDTO {
  @ApiProperty({ example: '2013-03-15' })
  @IsOptional()
  @IsString({ message: 'Fecha de Prestamo debe ser una Fecha Valida.' })
  fechaPrestamo: string;

  @ApiProperty({ example: '15:30' })
  @IsOptional()
  @IsString({ message: 'Hora de Prestamo debe ser una Hora Valida.' })
  horaPrestamo: string;

  @ApiProperty({ example: 'Esto es una prueba para las observaciones.' })
  @IsOptional()
  @IsString({ message: 'Observaciones debe ser cadena.' })
  obsPrestamo: string;

  @ApiProperty({ example: '2013-03-15' })
  @IsOptional()
  @IsString({ message: 'Fecha de Devolución debe ser una Fecha Valida.' })
  fechaDevolucion: string;

  @ApiProperty({ example: '15:30' })
  @IsOptional()
  @IsString({ message: 'Hora de Devolución debe ser una Hoa Valida.' })
  horaDevolucion: string;

  @ApiProperty({ example: 'Esto es una prueba para las observaciones.' })
  @IsOptional()
  @IsString({ message: 'Observaciones debe ser una cadena.' })
  obsDevolucion: string;

  @ApiProperty({ example: 'PRESTAMO' })
  @IsOptional({ message: 'Estado no debe estar vacio.' })
  @IsString({
    message: 'Estado debe ser una cadena, como: PRESTAMO o DEVUELTO',
  })
  estado: string;

  @ApiProperty({ example: true })
  @IsOptional()
  @IsBoolean({
    message: 'Activo debe ser de tipo Booleano, por ejemplo TRUE o False.',
  })
  activo: boolean;

  @ApiProperty({ example: 1 })
  @IsOptional()
  @IsNumber()
  persona: PersonasEntity;

  @ApiProperty({ example: 1 })
  @IsOptional()
  @IsNumber()
  tomo: TomoEntity;

  @ApiProperty({ example: 1 })
  @IsOptional()
  @IsNumber()
  medico: MedicoEntity;
}

export class PrestamoListadoDTO {
  @IsNotEmpty()
  data: PrestamoDTO[];

  @IsNotEmpty()
  @IsNumber()
  count: number;

  @IsNotEmpty()
  @IsNumber()
  page: number;

  @IsNotEmpty()
  @IsNumber()
  pageSize: number;
}

export class buscarDTO {
  afiliado: string;
  medico: string;
  estado: string;
}
