import { Module } from '@nestjs/common';
import { PrestamosService } from './services/prestamos.service';
import { PrestamosController } from './controllers/prestamos.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PrestamoEntity } from './entities/prestamo.entity';

@Module({
  imports: [TypeOrmModule.forFeature([PrestamoEntity])],
  controllers: [PrestamosController],
  providers: [PrestamosService],
  exports: [PrestamosModule],
})
export class PrestamosModule {}
