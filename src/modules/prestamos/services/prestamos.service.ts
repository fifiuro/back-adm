import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PrestamoEntity } from '../entities/prestamo.entity';
import { ILike, IsNull, Repository, UpdateResult } from 'typeorm';
import {
  PrestamoDTO,
  PrestamoListadoDTO,
  PrestamoUpdateDTO,
} from '../dtos/prestamo.dto';
import { ErrorManager } from 'src/core/shared/errorManager/error.manager';
import { PaginationQueryDto } from 'src/core/shared/dtos';
import { PaginationBuscarDto } from 'src/core/shared/dtos/pagination-query.dto';

@Injectable()
export class PrestamosService {
  constructor(
    @InjectRepository(PrestamoEntity)
    private readonly prestamoRepository: Repository<PrestamoEntity>,
  ) {}

  public async createPrestamo(body: PrestamoDTO): Promise<PrestamoEntity> {
    try {
      const duplicado = await this.checkIfPrestamoExists({
        key: 'fechaPrestamo',
        value: body.fechaPrestamo,
      });
      if (duplicado) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: 'El Tomo ya se presto.',
        });
        // throw new ErrorManager({
        //   type: 'BAD_REQUEST',
        //   message: `Ya existe el registro de Prestamo. con el valor: ${body.fechaPrestamo}`,
        // });
      } else {
        return await this.prestamoRepository.save(body);
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  private async checkIfPrestamoExists({
    key,
    value,
  }: {
    key: keyof PrestamoDTO;
    value: any;
  }): Promise<boolean> {
    const unidad: PrestamoEntity[] = await this.prestamoRepository.find({
      where: {
        [key]: value,
        estado: 'PRESTAMO',
      },
    });
    if (unidad.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  public async findPrestamo({
    limit,
    offset,
  }: PaginationQueryDto): Promise<any> {
    try {
      const take: number = limit || 10;
      const skip: number = (offset - 1) * take;
      const [result, total] = await this.prestamoRepository.findAndCount({
        order: { idPre: 'ASC' },
        take: take,
        skip: skip,
        relations: ['persona', 'tomo', 'medico'],
      });
      if (result.length === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: 'No se encontro resultados, de Prestamo.',
        });
      }
      return { data: result, count: total, page: offset, pageSize: limit };
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findPrestamoById(idPre: number): Promise<PrestamoEntity> {
    try {
      if (idPre > 0) {
        const prestamo: PrestamoEntity = await this.prestamoRepository.findOne({
          where: { idPre: idPre },
          relations: ['persona', 'tomo', 'medico'],
        });
        if (prestamo) {
          return prestamo;
        } else {
          throw new ErrorManager({
            type: 'BAD_REQUEST',
            message: `No encontramos Prestmo, con el ID: ${idPre}`,
          });
        }
      } else {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `El ID debe ser un número y no este valor: ${idPre}.`,
        });
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findPrestamoX3({
    limit,
    offset,
    afiliado,
    medico,
    estado,
  }: PaginationBuscarDto): Promise<any> {
    console.log(afiliado);
    console.log(medico);
    console.log(estado);
    let combinacion: string = '';
    if (afiliado) {
      combinacion += '1';
    } else {
      combinacion += '0';
    }
    if (medico) {
      combinacion += '1';
    } else {
      combinacion += '0';
    }
    if (estado === 'null') {
      combinacion += '0';
    } else {
      if (estado != '') {
        combinacion += '1';
      } else {
        combinacion += '0';
      }
    }
    console.log(combinacion);
    try {
      const take: number = limit || 10;
      const skip: number = (offset - 1) * take;
      switch (combinacion) {
        case '111':
          const [result, total] = await this.prestamoRepository.findAndCount({
            where: {
              persona: { nombreCompleto: ILike(`%${afiliado}%`) },
              medico: { nombres: ILike(`%${medico}%`) },
              estado: estado,
            },
            order: { idPre: 'ASC' },
            take: take,
            skip: skip,
            relations: ['persona', 'tomo', 'medico'],
          });
          if (result.length === 0) {
            throw new ErrorManager({
              type: 'BAD_REQUEST',
              message: 'No se encontro resultados, de Prestamo.',
            });
          }
          return { data: result, count: total, page: offset, pageSize: limit };
          break;
        case '110':
          const [result1, total1] = await this.prestamoRepository.findAndCount({
            where: {
              persona: { nombreCompleto: ILike(`%${afiliado}%`) },
              medico: { nombres: ILike(`%${medico}%`) },
            },
            order: { idPre: 'ASC' },
            take: take,
            skip: skip,
            relations: ['persona', 'tomo', 'medico'],
          });
          if (result1.length === 0) {
            throw new ErrorManager({
              type: 'BAD_REQUEST',
              message: 'No se encontro resultados, de Prestamo.',
            });
          }
          return {
            data: result1,
            count: total1,
            page: offset,
            pageSize: limit,
          };
          break;
        case '100':
          const [result2, total2] = await this.prestamoRepository.findAndCount({
            where: {
              persona: { nombreCompleto: ILike(`%${afiliado}%`) },
            },
            order: { idPre: 'ASC' },
            take: take,
            skip: skip,
            relations: ['persona', 'tomo', 'medico'],
          });
          if (result2.length === 0) {
            throw new ErrorManager({
              type: 'BAD_REQUEST',
              message: 'No se encontro resultados, de Prestamo.',
            });
          }
          return {
            data: result2,
            count: total2,
            page: offset,
            pageSize: limit,
          };
          break;
        case '010':
          const [result3, total3] = await this.prestamoRepository.findAndCount({
            where: {
              medico: { nombres: ILike(`%${medico}%`) },
            },
            order: { idPre: 'ASC' },
            take: take,
            skip: skip,
            relations: ['persona', 'tomo', 'medico'],
          });
          if (result3.length === 0) {
            throw new ErrorManager({
              type: 'BAD_REQUEST',
              message: 'No se encontro resultados, de Prestamo.',
            });
          }
          return {
            data: result3,
            count: total3,
            page: offset,
            pageSize: limit,
          };
          break;
        case '101':
          console.log('con el numero 101');
          const [result4, total4] = await this.prestamoRepository.findAndCount({
            where: {
              persona: { nombreCompleto: ILike(`%${afiliado}%`) },
              estado: estado,
            },
            order: { idPre: 'ASC' },
            take: take,
            skip: skip,
            relations: ['persona', 'tomo', 'medico'],
          });
          console.log(result4);
          if (result4.length === 0) {
            throw new ErrorManager({
              type: 'BAD_REQUEST',
              message: 'No se encontro resultados, de Prestamo.',
            });
          }
          return {
            data: result4,
            count: total4,
            page: offset,
            pageSize: limit,
          };
          break;
        case '011':
          const [result5, total5] = await this.prestamoRepository.findAndCount({
            where: {
              medico: { nombres: ILike(`%${medico}%`) },
              estado: estado,
            },
            order: { idPre: 'ASC' },
            take: take,
            skip: skip,
            relations: ['persona', 'tomo', 'medico'],
          });
          if (result5.length === 0) {
            throw new ErrorManager({
              type: 'BAD_REQUEST',
              message: 'No se encontro resultados, de Prestamo.',
            });
          }
          return {
            data: result5,
            count: total5,
            page: offset,
            pageSize: limit,
          };
          break;

        default:
          break;
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async updadePrestamo(
    body: PrestamoUpdateDTO,
    idPre: number,
  ): Promise<UpdateResult | undefined> {
    try {
      const prestamo: UpdateResult = await this.prestamoRepository.update(
        idPre,
        body,
      );
      if (prestamo.affected === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `No se encontro Prestamo, con el ID: ${idPre}. Para la modificación.`,
        });
      }
      return prestamo;
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async fecha(): Promise<any> {
    try {
      let f = new Date();
      var mes = String(f.getMonth() + 1);
      if (mes.length == 1) {
        mes = 0 + mes;
      }
      let fecha = f.getFullYear() + '-' + mes + '-' + f.getDate();
      let hora = f.getHours() + ':' + f.getMinutes();
      if (fecha && hora) {
        return { fecha: fecha, hora: hora };
      } else {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `La fecha o hora no se pudo obtener.`,
        });
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }
}
