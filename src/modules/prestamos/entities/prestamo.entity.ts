import { BaseEntity } from 'src/core/shared/entities/base.entity';
import { MedicoEntity } from 'src/modules/medicos/entities/medico.entity';
import { PersonasEntity } from 'src/modules/personas/entities/persona.entity';
import { TomoEntity } from 'src/modules/tomos/entities/tomo.entity';
import {
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  Timestamp,
} from 'typeorm';

@Entity('prestamos')
export class PrestamoEntity extends BaseEntity {
  @PrimaryGeneratedColumn({ name: 'id_pre' })
  idPre: number;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'fecha_prestamo',
    length:'50',
    unique: false,
  })
  fechaPrestamo: string;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'hora_prestamo',
    length: '50',
    unique: false,
  })
  horaPrestamo: string;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'obs_prestamo',
    length: '500',
    unique: false,
  })
  obsPrestamo: string;

  @Column({
    type: 'varchar',
    nullable: true,
    name: 'fecha_devolucion',
    length:'50',
    unique: false,
  })
  fechaDevolucion: Date;

  @Column({
    type: 'varchar',
    nullable: true,
    name: 'hora_devolucion',
    length: '50',
    unique: false,
  })
  horaDevolucion: string;

  @Column({
    type: 'varchar',
    nullable: true,
    name: 'obs_devolucion',
    length: '500',
    unique: false,
  })
  obsDevolucion: string;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'estado',
    length: '50',
    unique: false,
  })
  estado: string;

  @ManyToOne(() => PersonasEntity, (persona: PersonasEntity) => persona.idPer)
  persona: PersonasEntity;

  @ManyToOne(() => TomoEntity, (tomo: TomoEntity) => tomo.idTom)
  tomo: TomoEntity;

  @ManyToOne(() => MedicoEntity, (medico: MedicoEntity) => medico.idMed)
  medico: MedicoEntity;
}
