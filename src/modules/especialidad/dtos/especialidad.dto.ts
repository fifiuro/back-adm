import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class EspecialidadDTO {
  @ApiProperty({ example: 'Cardiologia' })
  @IsNotEmpty({ message: 'La Especialidad no debe estar vacío.' })
  @IsString({ message: 'La Especialidad debe ser de tipo cadena.' })
  especialidad: string;

  @ApiProperty({ example: true })
  @IsOptional()
  @IsBoolean({ message: 'La propiedad de Activo debe ser: Verdadero o Falso.' })
  activo: boolean;
}

export class EspecialidadUpdateDTO {
  @ApiProperty({ example: 'Cardiologia' })
  @IsOptional()
  @IsString({ message: 'La Especialidad debe ser de tipo cadena.' })
  especialidad: string;

  @ApiProperty({ example: true })
  @IsOptional()
  @IsBoolean({ message: 'La propiedad de Activo debe ser: Verdadero o Falso.' })
  activo: boolean;
}

export class EspecialidadListadoDTO {
  @IsNotEmpty()
  data: EspecialidadDTO[];

  @IsNotEmpty({ message: 'Conteo no debe ser vacío.' })
  @IsNumber()
  count: number;

  @IsNotEmpty({ message: 'Página no debe ser vacío.' })
  @IsNumber()
  page: number;

  @IsNotEmpty({ message: 'Número de elementos por Página no debe ser vacío.' })
  @IsNumber()
  pageSize: number;
}
