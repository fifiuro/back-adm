import { Module } from '@nestjs/common';
import { EspecialidadController } from './controllers/especialidad.controller';
import { EspecialidadService } from './services/especialidad.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EspecialidadEntity } from './entities/especialidad.entity';

@Module({
  imports: [TypeOrmModule.forFeature([EspecialidadEntity])],
  controllers: [EspecialidadController],
  providers: [EspecialidadService],
  exports: [EspecialidadModule],
})
export class EspecialidadModule {}
