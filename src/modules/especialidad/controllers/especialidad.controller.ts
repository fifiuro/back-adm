import { Body, Controller, Get, Param, Post, Put, Query, UseGuards } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { EspecialidadService } from '../services/especialidad.service';
import { EspecialidadDTO, EspecialidadUpdateDTO } from '../dtos/especialidad.dto';
import { PaginationQueryDto } from 'src/core/shared/dtos/pagination-query.dto';
import { AuthGuard } from 'src/core/guards/auth.guards';

@Controller('api/v1/especialidad')
@ApiTags('API Especialidad')
@UseGuards(AuthGuard)
export class EspecialidadController {
  constructor(private readonly especialidadService: EspecialidadService) {}
  @Post('add')
  @ApiOperation({ description: 'Crea un nuveo registro de Especialidad' })
  public async addEspecialidad(@Body() body: EspecialidadDTO) {
    return await this.especialidadService.createEspecialidad(body);
  }

  @Get('all')
  @ApiOperation({ description: 'Lista todos las Especialidades.' })
  public async allEspecialidades(@Query() pagination: PaginationQueryDto) {
    return await this.especialidadService.findEspecialidad(pagination);
  }

  @Get('like/:texto')
  @ApiOperation({
    description:
      'Lista todas las coincidencias con el texto enviado a Especialidad.',
  })
  public async likeEspecialidad(
    @Param('texto') texto: string,
    @Query() pagination: PaginationQueryDto,
  ) {
    return await this.especialidadService.findEspecialidadLike(
      pagination,
      texto,
    );
  }

  @Get('combo')
  @ApiOperation({
    description:
      'Lista todos las Especialidades pero con estado activo igual TRUE.',
  })
  public async comboEspecialidad() {
    return await this.especialidadService.findEspecialidadCombo();
  }

  @Get('id/:id')
  @ApiOperation({
    description: 'Devuelve la Especialidad segun el ID enviado.',
  })
  public async idEspecialidad(@Param('id') id: number) {
    return await this.especialidadService.findEspecialidadById(id);
  }

  @Put('update/:id')
  @ApiOperation({ description: 'Actualiza los datos de Especialidad.' })
  public async updateEspecialidad(
    @Param('id') id: number,
    @Body() body: EspecialidadUpdateDTO,
  ) {
    return await this.especialidadService.updadeEspecialidad(body, id);
  }
}
