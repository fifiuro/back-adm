import { BaseEntity } from 'src/core/shared/entities/base.entity';
import { CaratulaEntity } from 'src/modules/caratulas/entities/caratula.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity('especialidades')
export class EspecialidadEntity extends BaseEntity {
  @PrimaryGeneratedColumn({ name: 'id_esp' })
  idEsp: number;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'especialidad',
    length: '255',
    unique: false,
  })
  especialidad: string;

  @OneToMany(() => CaratulaEntity, (caratula: CaratulaEntity) => caratula.idCar)
  caratula: CaratulaEntity;
}
