import { Injectable } from '@nestjs/common';
import { EspecialidadEntity } from '../entities/especialidad.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { ILike, Repository, UpdateResult } from 'typeorm';
import {
  EspecialidadDTO,
  EspecialidadListadoDTO,
  EspecialidadUpdateDTO,
} from '../dtos/especialidad.dto';
import { ErrorManager } from 'src/core/shared/errorManager/error.manager';
import { PaginationQueryDto } from 'src/core/shared/dtos/pagination-query.dto';

@Injectable()
export class EspecialidadService {
  constructor(
    @InjectRepository(EspecialidadEntity)
    private readonly especialidadRepository: Repository<EspecialidadEntity>,
  ) {}

  public async createEspecialidad(
    body: EspecialidadDTO,
  ): Promise<EspecialidadEntity> {
    try {
      const duplicado = await this.checkIfEspecialidadExists({
        key: 'especialidad',
        value: body.especialidad,
      });
      if (duplicado) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `Ya existe el registro de Especialidad. con el valor: ${body.especialidad}`,
        });
      } else {
        return await this.especialidadRepository.save(body);
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  private async checkIfEspecialidadExists({
    key,
    value,
  }: {
    key: keyof EspecialidadDTO;
    value: string;
  }): Promise<boolean> {
    const especialidad: EspecialidadEntity[] =
      await this.especialidadRepository.find({
        where: {
          [key]: value,
        },
      });
    if (especialidad.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  public async findEspecialidad({
    limit,
    offset,
  }: PaginationQueryDto): Promise<EspecialidadListadoDTO> {
    try {
      const take: number = limit || 10;
      const skip: number = (offset - 1) * take;
      const [result, total] = await this.especialidadRepository.findAndCount({
        order: { idEsp: 'ASC' },
        take: take,
        skip: skip,
      });
      if (result.length === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: 'No se encontro resultados, de Especialidad.',
        });
      }
      return { data: result, count: total, page: offset, pageSize: limit };
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findEspecialidadLike(
    { limit, offset }: PaginationQueryDto,
    texto: string,
  ): Promise<EspecialidadListadoDTO> {
    try {
      const take = limit || 10;
      const skip = (offset - 1) * take;
      const [result, total] = await this.especialidadRepository.findAndCount({
        where: {
          especialidad: ILike(`%${texto}%`),
        },
        order: { idEsp: 'ASC' },
        take: take,
        skip: skip,
      });
      if (result.length === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `No se encontro resultados, con: ${texto}`,
        });
      }
      return { data: result, count: total, page: offset, pageSize: limit };
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findEspecialidadCombo(): Promise<EspecialidadEntity[]> {
    try {
      const especialidad: EspecialidadEntity[] =
        await this.especialidadRepository.find({ where: { activo: true } });
      if (especialidad.length === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: 'No se encontro resultado Especialidad.',
        });
      }
      return especialidad;
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findEspecialidadById(
    idEsp: number,
  ): Promise<EspecialidadEntity> {
    try {
      if (idEsp > 0) {
        const especialidad: EspecialidadEntity =
          await this.especialidadRepository.findOne({
            where: { idEsp: idEsp },
          });
        if (especialidad) {
          return especialidad;
        } else {
          throw new ErrorManager({
            type: 'BAD_REQUEST',
            message: `No encontramos Especialidad, con el ID: ${idEsp}`,
          });
        }
      } else {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `El ID debe ser un número y no este valor: ${idEsp}.`,
        });
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async updadeEspecialidad(
    body: EspecialidadUpdateDTO,
    idEsp: number,
  ): Promise<UpdateResult | undefined> {
    try {
      if (idEsp) {
        const especialidad: UpdateResult =
          await this.especialidadRepository.update(idEsp, body);
        if (especialidad.affected === 0) {
          throw new ErrorManager({
            type: 'BAD_REQUEST',
            message: `No se encontro Especialidad, con el ID: ${idEsp}. Para la modificación.`,
          });
        }
        return especialidad;
      } else {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `El ID no tiene ningun valor.`,
        });
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }
}
