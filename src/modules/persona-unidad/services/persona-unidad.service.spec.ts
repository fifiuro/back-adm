import { Test, TestingModule } from '@nestjs/testing';
import { PersonaUnidadService } from './persona-unidad.service';

describe('PersonaUnidadService', () => {
  let service: PersonaUnidadService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PersonaUnidadService],
    }).compile();

    service = module.get<PersonaUnidadService>(PersonaUnidadService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
