import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ILike, Repository, UpdateResult } from 'typeorm';
import { PersonaUnidadEntity } from '../entities/persona-unidad.entity';
import {
  PaginationBuscarDto,
  PersonaUnidadDTO,
  PersonaUnidadListadoDTO,
  PersonaUnidadUpdateDTO,
} from '../dtos/persona-unidad.dto';
import { ErrorManager } from 'src/core/shared/errorManager/error.manager';
import { PaginationQueryDto } from 'src/core/shared/dtos';
import { BuscarDto } from '../dtos/buscar/buscar.dto';

@Injectable()
export class PersonaUnidadService {
  constructor(
    @InjectRepository(PersonaUnidadEntity)
    private readonly personaUnidadRepository: Repository<PersonaUnidadEntity>,
  ) {}

  public async createPersonaUnidad(
    body: PersonaUnidadDTO,
  ): Promise<PersonaUnidadEntity> {
    try {
      const duplicado = await this.checkIfPersonaUnidadExists(
        body.persona,
        body.unidad,
      );
      if (duplicado) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `Ya existe el registro de Persona asignada a la Unidad. con el valor: ${body.persona}`,
        });
      } else {
        return await this.personaUnidadRepository.save(body);
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  private async checkIfPersonaUnidadExists(
    per: any,
    uni: any,
  ): Promise<boolean> {
    const perUni: PersonaUnidadEntity[] =
      await this.personaUnidadRepository.find({
        where: {
          persona: { idPer: per },
          unidad: { idUni: uni },
          activo: true,
        },
      });
    console.log(perUni.length);
    if (perUni.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  public async findPersonaUnidad({
    limit,
    offset,
  }: PaginationQueryDto): Promise<PersonaUnidadListadoDTO> {
    try {
      const take: number = limit || 10;
      const skip: number = (offset - 1) * take;
      const [result, total] = await this.personaUnidadRepository.findAndCount({
        order: { idPu: 'ASC' },
        take: take,
        skip: skip,
        relations: ['persona', 'unidad'],
      });
      if (result.length === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: 'No se encontro resultados, de Persona Unidad.',
        });
      }
      return { data: result, count: total, page: offset, pageSize: limit };
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }
  public async findPersonaUnidadLike({
    matricula,
    ci,
    limit,
    offset,
  }: PaginationBuscarDto): Promise<any> {
    try {
      const take = limit || 10;
      const skip = (offset - 1) * take;
      if (matricula && ci) {
        const [result, total] = await this.personaUnidadRepository.findAndCount(
          {
            where: {
              persona: {
                matriculaBeneficiario: ILike(`%${matricula}%`),
                ciBeneficiario: ILike(`%${ci}%`),
              },
            },
            order: { idPu: 'ASC' },
            take: take,
            skip: skip,
            relations: ['persona', 'unidad'],
          },
        );
        if (result.length === 0) {
          throw new ErrorManager({
            type: 'BAD_REQUEST',
            message: `No se encontro resultados, con: ${matricula}, ${ci}`,
          });
        }
        return { data: result, count: total, page: offset, pageSize: limit };
      } else if (matricula) {
        const [result, total] = await this.personaUnidadRepository.findAndCount(
          {
            where: {
              persona: [{ matriculaBeneficiario: ILike(`%${matricula}%`) }],
            },
            order: { idPu: 'ASC' },
            take: take,
            skip: skip,
            relations: ['persona', 'unidad'],
          },
        );
        if (result.length === 0) {
          throw new ErrorManager({
            type: 'BAD_REQUEST',
            message: `No se encontro resultados, con: ${matricula}`,
          });
        }
        return { data: result, count: total, page: offset, pageSize: limit };
      } else if (ci) {
        const [result, total] = await this.personaUnidadRepository.findAndCount(
          {
            where: {
              persona: [{ matriculaBeneficiario: ILike(`%${matricula}%`) }],
            },
            order: { idPu: 'ASC' },
            take: take,
            skip: skip,
            relations: ['persona', 'unidad'],
          },
        );
        if (result.length === 0) {
          throw new ErrorManager({
            type: 'BAD_REQUEST',
            message: `No se encontro resultados, con: ${ci}`,
          });
        }
        return { data: result, count: total, page: offset, pageSize: limit };
      }

      // const [result, total] = await this.personaUnidadRepository.findAndCount({
      //   where: {
      //     persona: [
      //       { matriculaBeneficiario: ILike(`%${matricula}%`) },
      //       { ciBeneficiario: ILike(`%${ci}%`) },
      //     ],
      //   },
      //   order: { idPu: 'ASC' },
      //   take: take,
      //   skip: skip,
      //   relations: ['persona', 'unidad'],
      // });

      // if (result.length === 0) {
      //   throw new ErrorManager({
      //     type: 'BAD_REQUEST',
      //     message: `No se encontro resultados, con: ${matricula}, ${ci}`,
      //   });
      // }
      // return { data: result, count: total, page: offset, pageSize: limit };
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
    return;
  }

  public async findPersonaUnidadById(
    idPu: number,
  ): Promise<PersonaUnidadEntity> {
    try {
      if (idPu > 0) {
        const perUni: PersonaUnidadEntity =
          await this.personaUnidadRepository.findOne({
            where: { idPu: idPu },
            relations: ['persona', 'unidad'],
          });
        if (perUni) {
          return perUni;
        } else {
          throw new ErrorManager({
            type: 'BAD_REQUEST',
            message: `No encontramos Personas Unidad, con el ID: ${idPu}`,
          });
        }
      } else {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `El ID debe ser un número y no este valor: ${idPu}.`,
        });
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async updatePersonaUnidad(
    body: PersonaUnidadUpdateDTO,
    idPu: number,
  ): Promise<UpdateResult | undefined> {
    try {
      const unidad: UpdateResult = await this.personaUnidadRepository.update(
        idPu,
        body,
      );
      if (unidad.affected === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `No se encontro Persona Unidad, con el ID: ${idPu}. Para la modificación.`,
        });
      }
      return unidad;
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }
}
