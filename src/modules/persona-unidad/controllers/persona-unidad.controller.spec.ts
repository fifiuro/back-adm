import { Test, TestingModule } from '@nestjs/testing';
import { PersonaUnidadController } from './persona-unidad.controller';

describe('PersonaUnidadController', () => {
  let controller: PersonaUnidadController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PersonaUnidadController],
    }).compile();

    controller = module.get<PersonaUnidadController>(PersonaUnidadController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
