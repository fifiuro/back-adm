import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { PersonaUnidadService } from '../services/persona-unidad.service';
import {
  PaginationBuscarDto,
  PersonaUnidadDTO,
  PersonaUnidadUpdateDTO,
} from '../dtos/persona-unidad.dto';
import { PaginationQueryDto } from 'src/core/shared/dtos';
import { BuscarDto } from '../dtos/buscar/buscar.dto';
import { AuthGuard } from 'src/core/guards/auth.guards';

@Controller('api/v1/persona-unidad')
@ApiTags('API PersonaUnidad')
@UseGuards(AuthGuard)
export class PersonaUnidadController {
  constructor(private readonly personaUnidadService: PersonaUnidadService) {}

  @Post('add')
  @ApiOperation({
    description: 'Crea un nuevo registro de Persona a la Unidad que pertenece.',
  })
  public async addPersonaUnidad(@Body() body: PersonaUnidadDTO) {
    return await this.personaUnidadService.createPersonaUnidad(body);
  }

  @Get('all')
  @ApiOperation({ description: 'Lista todos las Persona con Unidades.' })
  public async allPersonaUnidad(@Query() pagination: PaginationQueryDto) {
    return await this.personaUnidadService.findPersonaUnidad(pagination);
  }

  @Get('like')
  @ApiOperation({
    description:
      'Lista todas las conincidencias con el texto enviado a Persona Unidad.',
  })
  public async likePersonaUnidad(@Query() pagination: PaginationBuscarDto) {
    return await this.personaUnidadService.findPersonaUnidadLike(pagination);
  }

  @Get('id/:id')
  @ApiOperation({
    description: 'Devuelve la Persona con Unidad segun el ID enviado.',
  })
  public async idPersonaUnidad(@Param('id') id: number) {
    return await this.personaUnidadService.findPersonaUnidadById(id);
  }

  @Put('update/:id')
  @ApiOperation({ description: 'Actualiza los datos de Persona a la Unidad.' })
  public async updatePersonaUnidad(
    @Param('id') id: number,
    @Body() body: PersonaUnidadUpdateDTO,
  ) {
    return await this.personaUnidadService.updatePersonaUnidad(body, id);
  }
}
