import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsBoolean,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsPositive,
  IsString,
} from 'class-validator';
import { PersonasEntity } from 'src/modules/personas/entities/persona.entity';
import { UnidadEntity } from 'src/modules/unidad/entities/unidad.entity';

export class PersonaUnidadDTO {
  @ApiProperty({ example: true })
  @IsOptional()
  @IsBoolean()
  activo: boolean;

  @ApiProperty({ example: 1 })
  @IsNotEmpty()
  @IsNumber()
  persona: PersonasEntity;

  @ApiProperty({ example: 1 })
  @IsNotEmpty()
  @IsNumber()
  unidad: UnidadEntity;
}

export class PersonaUnidadUpdateDTO {
  @ApiProperty({ example: true })
  @IsOptional()
  @IsBoolean()
  activo: boolean;

  @ApiProperty({ example: 1 })
  @IsOptional()
  @IsNumber()
  persona: PersonasEntity;

  @ApiProperty({ example: 1 })
  @IsOptional()
  @IsNumber()
  unidad: UnidadEntity;
}

export class PersonaUnidadListadoDTO {
  @IsNotEmpty()
  data: PersonaUnidadDTO[];

  @IsNotEmpty()
  @IsNumber()
  count: number;

  @IsNotEmpty()
  @IsNumber()
  page: number;

  @IsNotEmpty()
  @IsNumber()
  pageSize: number;
}

export class PaginationBuscarDto {
  @ApiProperty({ example: '90-5501 TIA' })
  @IsOptional()
  @IsString()
  matricula: string;

  @ApiProperty({ example: '7033463' })
  @IsOptional()
  @IsString()
  ci: string;

  @ApiProperty({ example: 10 })
  @Type(() => Number)
  @IsPositive()
  @IsOptional()
  limit: number;

  @ApiProperty({ example: 1 })
  @Type(() => Number)
  @IsPositive()
  @IsOptional()
  offset: number;
}
