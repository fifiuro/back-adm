import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsString } from 'class-validator';

export class BuscarDto {
  @ApiProperty({ example: 'Pepe' })
  @IsOptional()
  @IsString()
  nombre: string;

  @ApiProperty({ example: '90-5501 TIA' })
  @IsOptional()
  @IsString()
  matricula: string;

  @ApiProperty({ example: '7033463' })
  @IsOptional()
  @IsString()
  ci: string;
}

export class BuscarLikeDto {
  data: [];
  count: number;
  page: number;
  pageSize: number;
}
