import { Module } from '@nestjs/common';
import { PersonaUnidadService } from './services/persona-unidad.service';
import { PersonaUnidadController } from './controllers/persona-unidad.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PersonaUnidadEntity } from './entities/persona-unidad.entity';

@Module({
  imports: [TypeOrmModule.forFeature([PersonaUnidadEntity])],
  controllers: [PersonaUnidadController],
  providers: [PersonaUnidadService],
  exports: [PersonaUnidadModule],
})
export class PersonaUnidadModule {}
