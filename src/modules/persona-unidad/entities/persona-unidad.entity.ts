import { BaseEntity } from 'src/core/shared/entities/base.entity';
import { PersonasEntity } from 'src/modules/personas/entities/persona.entity';
import { UnidadEntity } from 'src/modules/unidad/entities/unidad.entity';
import { Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity('persona_unidad')
export class PersonaUnidadEntity extends BaseEntity {
  @PrimaryGeneratedColumn({ name: 'id_pu' })
  idPu: number;

  @ManyToOne(() => PersonasEntity, (persona: PersonasEntity) => persona.idPer)
  persona: PersonasEntity;

  @ManyToOne(() => UnidadEntity, (unidad: UnidadEntity) => unidad.idUni)
  unidad: UnidadEntity;
}
