import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { PaginationQueryDto } from 'src/core/shared/dtos/pagination-query.dto';
import { CarpetaService } from '../services/carpeta.service';
import { CarpetaDTO, CarpetaUpdateDTO } from '../dtos/carperta.dto';
import { AuthGuard } from 'src/core/guards/auth.guards';

@Controller('api/v1/carpeta')
@ApiTags('API Carpeta')
@UseGuards(AuthGuard)
export class CarpetaController {
  constructor(private readonly carpetaService: CarpetaService) {}

  @Post('add')
  @ApiOperation({ description: 'Crea un nuevo registro de Carpeta.' })
  public async addCarpeta(@Body() body: CarpetaDTO) {
    return await this.carpetaService.createCarpeta(body);
  }

  @Get('all')
  @ApiOperation({ description: 'Lista todos las Carpetas.' })
  public async allCarpeta(@Query() pagination: PaginationQueryDto) {
    return await this.carpetaService.findCarpeta(pagination);
  }

  @Get('like/:texto')
  @ApiOperation({
    description:
      'Lista todas las coincidencias con el texto enviado a Carpeta.',
  })
  public async likeCarpeta(
    @Param('texto') texto: string,
    @Query() pagination: PaginationQueryDto,
  ) {
    return await this.carpetaService.findCarpetaLike(pagination, texto);
  }

  @Get('combo')
  @ApiOperation({
    description: 'Lista todos las Carpetas pero con estado activo igual TRUE.',
  })
  public async comboCarpeta() {
    return await this.carpetaService.findCarpetaCombo();
  }

  @Get('id/:id')
  @ApiOperation({
    description: 'Devuelve la Carpeta segun el ID enviado.',
  })
  public async idCarpeta(@Param('id') id: number) {
    return await this.carpetaService.findCarpetaById(id);
  }

  @Put('update/:id')
  @ApiOperation({ description: 'Actualiza los datos de Carpeta.' })
  public async updateCarpeta(
    @Param('id') id: number,
    @Body() body: CarpetaUpdateDTO,
  ) {
    return await this.carpetaService.updadeCarpeta(body, id);
  }
}
