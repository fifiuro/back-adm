import { Test, TestingModule } from '@nestjs/testing';
import { CarpetaController } from './carpeta.controller';

describe('CarpetaController', () => {
  let controller: CarpetaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CarpetaController],
    }).compile();

    controller = module.get<CarpetaController>(CarpetaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
