import { Test, TestingModule } from '@nestjs/testing';
import { CareptaService } from './carpeta.service';

describe('CareptaService', () => {
  let service: CareptaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CareptaService],
    }).compile();

    service = module.get<CareptaService>(CareptaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
