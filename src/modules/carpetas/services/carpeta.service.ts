import { Injectable } from '@nestjs/common';
import { CarpetaEntity } from '../entities/carpeta.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { ILike, Repository, UpdateResult } from 'typeorm';
import {
  CarpetaDTO,
  CarpetaListadoDTO,
  CarpetaUpdateDTO,
} from '../dtos/carperta.dto';
import { ErrorManager } from 'src/core/shared/errorManager/error.manager';
import { PaginationQueryDto } from 'src/core/shared/dtos';

@Injectable()
export class CarpetaService {
  constructor(
    @InjectRepository(CarpetaEntity)
    private readonly carpetaRepository: Repository<CarpetaEntity>,
  ) {}

  public async createCarpeta(body: CarpetaDTO): Promise<CarpetaEntity> {
    let pjson = JSON.stringify(body.estructura);
    let uno = JSON.parse(pjson)
    body.estructura = uno;

    try {
      return await this.carpetaRepository.save(body);
    } catch (error) {
      throw new ErrorManager({
        type: 'BAD_REQUEST',
        message: `no se pudo Realizar el Registro de Carpeta.`,
      });
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findCarpeta({
    limit,
    offset,
  }: PaginationQueryDto): Promise<CarpetaListadoDTO> {
    try {
      const take: number = limit || 10;
      const skip: number = (offset - 1) * take;
      const [result, total] = await this.carpetaRepository.findAndCount({
        order: { idCar: 'ASC' },
        take: take,
        skip: skip,
        relations: ['unidad'],
      });
      if (result.length === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: 'No se encontro resultados, de Carpeta.',
        });
      }
      return { data: result, count: total, page: offset, pageSize: limit };
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findCarpetaLike(
    { limit, offset }: PaginationQueryDto,
    texto: string,
  ): Promise<CarpetaListadoDTO> {
    try {
      const take = limit || 10;
      const skip = (offset - 1) * take;
      const [result, total] = await this.carpetaRepository.findAndCount({
        where: {
          unidad: { unidad: ILike(`%${texto}%`) },
        },
        order: { idCar: 'ASC' },
        take: take,
        skip: skip,
        relations: ['unidad'],
      });
      if (result.length === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `No se encontro resultados, con: ${texto}`,
        });
      }
      return { data: result, count: total, page: offset, pageSize: limit };
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findCarpetaCombo(): Promise<CarpetaEntity[]> {
    try {
      const unidad: CarpetaEntity[] = await this.carpetaRepository.find({
        where: { activo: true },
        relations: ['unidad'],
      });
      if (unidad.length === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: 'No se encontro resultado Carpeta.',
        });
      }
      return unidad;
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findCarpetaById(idCar: number): Promise<CarpetaEntity> {
    try {
      if (idCar > 0) {
        const carpeta: CarpetaEntity = await this.carpetaRepository.findOne({
          where: { idCar: idCar },
          relations: ['unidad'],
        });
        if (carpeta) {
          return carpeta;
        } else {
          throw new ErrorManager({
            type: 'BAD_REQUEST',
            message: `No encontramos Carpeta, con el ID: ${idCar}`,
          });
        }
      } else {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `El ID debe ser un número y no este valor: ${idCar}.`,
        });
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async updadeCarpeta(
    body: CarpetaUpdateDTO,
    idCar: number,
  ): Promise<UpdateResult | undefined> {
    try {
      const carpeta: UpdateResult = await this.carpetaRepository.update(
        idCar,
        body,
      );
      if (carpeta.affected === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `No se encontro Carpeta, con el ID: ${idCar}. Para la modificación.`,
        });
      }
      return carpeta;
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }
}
