import { ApiProperty } from '@nestjs/swagger';
import { BaseEntity } from 'src/core/shared/entities/base.entity';
import { UnidadEntity } from 'src/modules/unidad/entities/unidad.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity('carpetas')
export class CarpetaEntity extends BaseEntity {
  @PrimaryGeneratedColumn({ name: 'id_car' })
  idCar: number;

  @Column({
    type: 'jsonb',
    nullable: false,
    name: 'estructura',
    unique: false,
  })
  estructura: Record<string, any>;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'ruta',
    length: '200',
    unique: false,
  })
  ruta: string;
  
  @ManyToOne(() => UnidadEntity, (unidad: UnidadEntity) => unidad.idUni)
  unidad: UnidadEntity;
}
