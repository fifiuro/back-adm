import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsNotEmpty,
  IsNumber,
  IsObject,
  IsOptional,
  IsString,
} from 'class-validator';
import { UnidadEntity } from 'src/modules/unidad/entities/unidad.entity';

export class CarpetaDTO {
  @ApiProperty({
    example:
      '{ Carpeta: “matricula | ci paciente”, Subcarpeta: [{ nombreSubCarpeta: “nombreCarpeta1” ,Subcarpeta: [] } { nombreSubCarpeta: “nombreCarpeta2”, Subcarpeta: [] } { nombreSubCarpeta: “nombreCarpeta3”, Subcarpeta: [] } { nombreSubCarpeta: “nombreCarpeta4” } { nombreSubCarpeta: “nombreCarpeta5” Subcarpeta: [ { nombreSubCarpeta: “nombreCarpeta1” } { nombreSubCarpeta: “nombreCarpeta1” } { nombreSubCarpeta: “nombreCarpeta1” } ] } ] }',
  })
  @IsNotEmpty({ message: 'La Estructura no debe estar vacío.' })
  @IsString({ message: 'Estructura debe ser una Cadena.' })
  estructura: Record<string, any>;

  @ApiProperty({ example: 1 })
  @IsNotEmpty()
  @IsNumber()
  unidad: UnidadEntity;

  @ApiProperty({ example: 1 })
  @IsNotEmpty()
  @IsString({ message: 'La Ruta debe ser de tipo cadena.' })
  ruta: string;

  @ApiProperty({ example: true })
  @IsOptional()
  @IsBoolean({ message: 'La propiedad de Activo debe ser: Verdadero o Falso.' })
  activo: boolean;
}

export class CarpetaUpdateDTO {
  @ApiProperty({
    example:
      '{ Carpeta: “matricula | ci paciente”, Subcarpeta: [{ nombreSubCarpeta: “nombreCarpeta1” ,Subcarpeta: [] } { nombreSubCarpeta: “nombreCarpeta2”, Subcarpeta: [] } { nombreSubCarpeta: “nombreCarpeta3”, Subcarpeta: [] } { nombreSubCarpeta: “nombreCarpeta4” } { nombreSubCarpeta: “nombreCarpeta5” Subcarpeta: [ { nombreSubCarpeta: “nombreCarpeta1” } { nombreSubCarpeta: “nombreCarpeta1” } { nombreSubCarpeta: “nombreCarpeta1” } ] } ] }',
  })
  @IsOptional()
  @IsString({ message: 'Estructura debe ser una Cadena.' })
  estructura: Record<string, any>;

  @ApiProperty({ example: 1 })
  @IsOptional()
  @IsNumber()
  unidad: UnidadEntity;

  @ApiProperty({ example: 1 })
  @IsOptional()
  @IsString({ message: 'La Ruta debe ser de tipo cadena.' })
  ruta: string;

  @ApiProperty({ example: true })
  @IsOptional()
  @IsBoolean({ message: 'La propiedad de Activo debe ser: Verdadero o Falso.' })
  activo: boolean;
}

export class CarpetaListadoDTO {
  @IsNotEmpty()
  data: CarpetaDTO[];

  @IsNotEmpty()
  @IsNumber()
  count: number;

  @IsNotEmpty()
  @IsNumber()
  page: number;

  @IsNotEmpty()
  @IsNumber()
  pageSize: number;
}
