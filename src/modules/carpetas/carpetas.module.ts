import { Module } from '@nestjs/common';
import { CarpetaController } from './controllers/carpeta.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CarpetaEntity } from './entities/carpeta.entity';
import { CarpetaService } from './services/carpeta.service';

@Module({
  imports: [TypeOrmModule.forFeature([CarpetaEntity])],
  controllers: [CarpetaController],
  providers: [CarpetaService],
  exports:[CarpetasModule]  
})
export class CarpetasModule {}
