import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class UnidadDTO {
  @ApiProperty({ example: 'Archivos Clinicos' })
  @IsNotEmpty({ message: 'La Unidad no debe estar vacío.' })
  @IsString({ message: 'La Unidad debe ser de tipo cadena.' })
  unidad: string;

  @ApiProperty({ example: 'ACL' })
  @IsNotEmpty({ message: 'La Sigla no debe estar vacío.' })
  @IsString({ message: 'La Sigla debe ser de tipo cadena.' })
  sigla: string;

  @ApiProperty({ example: true })
  @IsOptional()
  @IsBoolean({ message: 'La propiedad de Activo debe ser: Verdadero o Falso.' })
  activo: boolean;
}

export class UnidadUpdateDTO {
  @ApiProperty({ example: 'Archivos Clinicos' })
  @IsOptional({ message: 'La Unidad no debe estar vacío.' })
  @IsString({ message: 'La Unidad debe ser de tipo cadena.' })
  unidad: string;

  @ApiProperty({ example: 'ACL' })
  @IsOptional({ message: 'La Sigla no debe estar vacío.' })
  @IsString({ message: 'La Sigla debe ser de tipo cadena.' })
  sigla: string;

  @ApiProperty({ example: true })
  @IsOptional()
  @IsBoolean({ message: 'La propiedad de Activo debe ser: Verdadero o Falso.' })
  activo: boolean;
}

export class UnidadListadoDTO {
  @IsNotEmpty()
  data: UnidadDTO[];

  @IsNotEmpty({ message: 'Conteo no debe ser vacío.' })
  @IsNumber()
  count: number;

  @IsNotEmpty({ message: 'Página no debe ser vacío.' })
  @IsNumber()
  page: number;

  @IsNotEmpty({ message: 'Número de elementos por Página no debe ser vacío.' })
  @IsNumber()
  pageSize: number;
}
