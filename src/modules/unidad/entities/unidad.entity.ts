import { BaseEntity } from 'src/core/shared/entities/base.entity';
import { CarpetaEntity } from 'src/modules/carpetas/entities/carpeta.entity';
import { PersonaUnidadEntity } from 'src/modules/persona-unidad/entities/persona-unidad.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity('unidad')
export class UnidadEntity extends BaseEntity {
  @PrimaryGeneratedColumn({ name: 'id_uni' })
  idUni: number;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'unidad',
    length: '255',
    unique: false,
  })
  unidad: string;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'sigla',
    length: '10',
    unique: false,
  })
  sigla: string;

  @OneToMany(() => CarpetaEntity, (carpeta: CarpetaEntity) => carpeta.idCar)
  carpeta: CarpetaEntity[];

  @OneToMany(
    () => PersonaUnidadEntity,
    (personaUnidad: PersonaUnidadEntity) => personaUnidad.idPu,
  )
  personaUnidad: PersonaUnidadEntity[];
}
