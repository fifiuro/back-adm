import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ILike, Repository, UpdateResult } from 'typeorm';
import { UnidadEntity } from '../entities/unidad.entity';
import {
  UnidadDTO,
  UnidadListadoDTO,
  UnidadUpdateDTO,
} from '../dtos/unidad.dto';
import { ErrorManager } from 'src/core/shared/errorManager/error.manager';
import { PaginationQueryDto } from 'src/core/shared/dtos/';

@Injectable()
export class UnidadService {
  constructor(
    @InjectRepository(UnidadEntity)
    private readonly unidadRepository: Repository<UnidadEntity>,
  ) {}

  public async createUnidad(body: UnidadDTO): Promise<UnidadEntity> {
    try {
      const duplicado = await this.checkIfUnidadExists({
        key: 'unidad',
        value: body.unidad,
      });
      if (duplicado) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `Ya existe el registro de Tipo de Atención. con el valor: ${body.unidad}`,
        });
      } else {
        return await this.unidadRepository.save(body);
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  private async checkIfUnidadExists({
    key,
    value,
  }: {
    key: keyof UnidadDTO;
    value: string;
  }): Promise<boolean> {
    const unidad: UnidadEntity[] = await this.unidadRepository.find({
      where: {
        [key]: value,
      },
    });
    if (unidad.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  public async findUnidad({
    limit,
    offset,
  }: PaginationQueryDto): Promise<UnidadListadoDTO> {
    try {
      const take: number = limit || 10;
      const skip: number = (offset - 1) * take;
      const [result, total] = await this.unidadRepository.findAndCount({
        order: { idUni: 'ASC' },
        take: take,
        skip: skip,
      });
      if (result.length === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: 'No se encontro resultados, de Unidad.',
        });
      }
      return { data: result, count: total, page: offset, pageSize: limit };
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findUnidadLike(
    { limit, offset }: PaginationQueryDto,
    texto: string,
  ): Promise<UnidadListadoDTO> {
    try {
      const take = limit || 10;
      const skip = (offset - 1) * take;
      const [result, total] = await this.unidadRepository.findAndCount({
        where: {
          unidad: ILike(`%${texto}%`),
        },
        order: { idUni: 'ASC' },
        take: take,
        skip: skip,
      });
      if (result.length === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `No se encontro resultados, con: ${texto}`,
        });
      }
      return { data: result, count: total, page: offset, pageSize: limit };
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findUnidadCombo(): Promise<UnidadEntity[]> {
    try {
      const unidad: UnidadEntity[] = await this.unidadRepository.find({
        where: { activo: true },
      });
      if (unidad.length === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: 'No se encontro resultado Unidad.',
        });
      }
      return unidad;
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findUnidadById(idTip: number): Promise<UnidadEntity> {
    try {
      if (idTip > 0) {
        const unidad: UnidadEntity = await this.unidadRepository.findOne({
          where: { idUni: idTip },
        });
        if (unidad) {
          return unidad;
        } else {
          throw new ErrorManager({
            type: 'BAD_REQUEST',
            message: `No encontramos Unidad, con el ID: ${idTip}`,
          });
        }
      } else {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `El ID debe ser un número y no este valor: ${idTip}.`,
        });
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async updadeUnidad(
    body: UnidadUpdateDTO,
    idUni: number,
  ): Promise<UpdateResult | undefined> {
    try {
      if (idUni) {
        const unidad: UpdateResult = await this.unidadRepository.update(
          idUni,
          body,
        );
        if (unidad.affected === 0) {
          throw new ErrorManager({
            type: 'BAD_REQUEST',
            message: `No se encontro Unidad, con el ID: ${idUni}. Para la modificación.`,
          });
        }
        return unidad;
      } else {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `El ID no tiene ningun valor.`,
        });
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }
}
