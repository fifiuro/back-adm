import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { UnidadService } from '../services/unidad.service';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { UnidadDTO, UnidadUpdateDTO } from '../dtos/unidad.dto';
import { PaginationQueryDto } from 'src/core/shared/dtos/pagination-query.dto';
import { AuthGuard } from 'src/core/guards/auth.guards';

@Controller('api/v1/unidad')
@ApiTags('API Unidad')
@UseGuards(AuthGuard)
export class UnidadController {
  constructor(private readonly unidadService: UnidadService) {}
  @Post('add')
  @ApiOperation({ description: 'Crea un nuevo registro de Unidad.' })
  public async addUnidad(@Body() body: UnidadDTO) {
    return await this.unidadService.createUnidad(body);
  }

  @Get('all')
  @ApiOperation({ description: 'Lista todos las Unidades.' })
  public async allUnidad(@Query() pagination: PaginationQueryDto) {
    return await this.unidadService.findUnidad(pagination);
  }

  @Get('like/:texto')
  @ApiOperation({
    description: 'Lista todas las coincidencias con el texto enviado a Unidad.',
  })
  public async likeUnidad(
    @Param('texto') texto: string,
    @Query() pagination: PaginationQueryDto,
  ) {
    return await this.unidadService.findUnidadLike(pagination, texto);
  }

  @Get('combo')
  @ApiOperation({
    description: 'Lista todos las Unidades pero con estado activo igual TRUE.',
  })
  public async comboUnidad() {
    return await this.unidadService.findUnidadCombo();
  }

  @Get('id/:id')
  @ApiOperation({
    description: 'Devuelve la Unidad segun el ID enviado.',
  })
  public async idUnidad(@Param('id') id: number) {
    return await this.unidadService.findUnidadById(id);
  }

  @Put('update/:id')
  @ApiOperation({ description: 'Actualiza los datos de Unidad.' })
  public async updateUnidad(
    @Param('id') id: number,
    @Body() body: UnidadUpdateDTO,
  ) {
    return await this.unidadService.updadeUnidad(body, id);
  }
}
