import { Module } from '@nestjs/common';
import { UnidadService } from './services/unidad.service';
import { UnidadController } from './controllers/unidad.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UnidadEntity } from './entities/unidad.entity';

@Module({
  imports: [TypeOrmModule.forFeature([UnidadEntity])],
  controllers: [UnidadController],
  providers: [UnidadService],
  exports: [UnidadModule],
})
export class UnidadModule {}
