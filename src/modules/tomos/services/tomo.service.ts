import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TomoEntity } from '../entities/tomo.entity';
import { Repository, UpdateResult } from 'typeorm';
import { TomoDTO, TomoListadoDTO, TomoUpdateDTO } from '../dtos/tomo.dto';
import { ErrorManager } from 'src/core/shared/errorManager/error.manager';
import { PaginationQueryDto } from 'src/core/shared/dtos';

@Injectable()
export class TomoService {
  constructor(
    @InjectRepository(TomoEntity)
    private readonly tomoRepository: Repository<TomoEntity>,
  ) {}

  public async createTomo(body: TomoDTO): Promise<TomoEntity> {
    try {
      let id: any = body.persona;
      const tomo = await this.checkIfTomoExists(id, body.tomo);
      if (tomo) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `Ya existe el registro de Tipo de Atención. con el valor: ${body.tomo}`,
        });
      } else {
        return await this.tomoRepository.save(body);
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  private async checkIfTomoExists(id: number, t: string): Promise<boolean> {
    console.log('Desde el Metodo de Verificacion....');
    console.log(id);
    console.log(t);
    const tomo: TomoEntity[] = await this.tomoRepository.find({
      where: {
        tomo: t,
        persona: { idPer: id },
      },
    });
    // const tomo: TomoEntity[] = await this.tomoRepository.find({
    //   where: {
    //     [key]: value,
    //   },
    // });
    console.log(tomo);
    if (tomo.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  public async findTomo({
    limit,
    offset,
  }: PaginationQueryDto): Promise<TomoListadoDTO> {
    try {
      const take: number = limit || 10;
      const skip: number = (offset - 1) * take;
      const [result, total] = await this.tomoRepository.findAndCount({
        order: { idTom: 'ASC' },
        take: take,
        skip: skip,
        relations: ['persona'],
      });
      if (result.length === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: 'No se encontro resultados, de Tomo.',
        });
      }
      return { data: result, count: total, page: offset, pageSize: limit };
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findTomoCombo(id: number): Promise<TomoEntity[]> {
    try {
      const tomo: TomoEntity[] = await this.tomoRepository.find({
        where: { activo: true, persona: { idPer: id } },
      });
      if (tomo.length === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: 'No se encontro resultado Tomo.',
        });
      }
      return tomo;
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findTomoById(idTom: number): Promise<TomoEntity> {
    try {
      if (idTom > 0) {
        const tomo: TomoEntity = await this.tomoRepository.findOne({
          where: { idTom: idTom },
          relations: ['persona'],
        });
        if (tomo) {
          return tomo;
        } else {
          throw new ErrorManager({
            type: 'BAD_REQUEST',
            message: `No encontramos Tomo, con el ID: ${idTom}`,
          });
        }
      } else {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `El ID debe ser un número y no este valor: ${idTom}.`,
        });
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async findTomoByIdByPersona(idPer: number): Promise<TomoEntity> {
    try {
      if (idPer > 0) {
        const tomo: TomoEntity = await this.tomoRepository.findOne({
          where: { persona: { idPer: idPer } },
          order: { numero: 'DESC' },
          relations: ['persona'],
        });
        if (tomo) {
          return tomo;
        } else {
          throw new ErrorManager({
            type: 'BAD_REQUEST',
            message: `No encontramos Tomo, con el ID: ${idPer}`,
          });
        }
      } else {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `El ID debe ser un número y no este valor: ${idPer}.`,
        });
      }
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async updadeTomo(
    body: TomoUpdateDTO,
    idTom: number,
  ): Promise<UpdateResult | undefined> {
    try {
      const tomo: UpdateResult = await this.tomoRepository.update(idTom, body);
      if (tomo.affected === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: `No se encontro Tomo, con el ID: ${idTom}. Para la modificación.`,
        });
      }
      return tomo;
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }

  public async updateFojas(
    body: TomoUpdateDTO,
    id: number,
  ): Promise<UpdateResult | undefined> {
    try {
      const t: TomoEntity = await this.tomoRepository.findOne({
        where: { idTom: id },
      });

      console.log('%%%%%%%%%%%%%%%%%%%%');
      console.log(t.fojas);
      let f = body.fojas + t.fojas;
      body.fojas = f;

      console.log(body);
      console.log('%%%%%%%%%%%%%%%%%%%%');

      const tomo: UpdateResult = await this.tomoRepository.update(id, body);
      if (tomo.affected === 0) {
        throw new ErrorManager({
          type: 'BAD_REQUEST',
          message: 'No se pudo realizar la suma de fojas.',
        });
      }
      return tomo;
    } catch (error) {
      throw ErrorManager.createSignatureError(error.message);
    }
  }
}
