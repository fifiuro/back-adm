import { Test, TestingModule } from '@nestjs/testing';
import { TomoService } from './tomo.service';

describe('TomoService', () => {
  let service: TomoService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TomoService],
    }).compile();

    service = module.get<TomoService>(TomoService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
