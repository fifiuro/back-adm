import { Module } from '@nestjs/common';
import { TomoService } from './services/tomo.service';
import { TomoController } from './controllers/tomo.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TomoEntity } from './entities/tomo.entity';

@Module({
  imports: [TypeOrmModule.forFeature([TomoEntity])],
  controllers: [TomoController],
  providers: [TomoService],
  exports: [TomosModule],
})
export class TomosModule {}
