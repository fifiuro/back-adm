import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import { PersonasEntity } from 'src/modules/personas/entities/persona.entity';

export class TomoDTO {
  @ApiProperty({ example: 'Tomo1' })
  @IsNotEmpty()
  @IsString()
  tomo: string;

  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  @IsNumber()
  fojas: number;

  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  @IsNumber()
  cd: number;

  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  @IsNumber()
  rx: number;

  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  @IsNumber()
  tomografia: number;

  @ApiProperty({ example: 'ABIERTO' })
  @IsNotEmpty()
  @IsString()
  estado: string;

  @ApiProperty({ example: 1 })
  @IsNotEmpty()
  @IsNumber()
  numero: number;

  @ApiProperty({ example: 1 })
  @IsNotEmpty()
  @IsNumber()
  persona: PersonasEntity;

  @ApiProperty({ example: true })
  @IsOptional()
  @IsBoolean()
  activo: boolean;
}

export class TomoUpdateDTO {
  @ApiProperty({ example: 'Tomo1' })
  @IsOptional()
  @IsString()
  tomo: string;

  @ApiProperty({ example: '1' })
  @IsOptional()
  @IsNumber()
  fojas: number;

  @ApiProperty({ example: '1' })
  @IsOptional()
  @IsNumber()
  cd: number;

  @ApiProperty({ example: '1' })
  @IsOptional()
  @IsNumber()
  rx: number;

  @ApiProperty({ example: '1' })
  @IsOptional()
  @IsNumber()
  tomografia: number;

  @ApiProperty({ example: 'ABIERTO' })
  @IsOptional()
  @IsString()
  estado: string;

  @ApiProperty({ example: 1 })
  @IsOptional()
  @IsNumber()
  numero: number;

  @ApiProperty({ example: 1 })
  @IsOptional()
  @IsNumber()
  persona: PersonasEntity;

  @ApiProperty({ example: true })
  @IsOptional()
  @IsBoolean()
  activo: boolean;
}

export class TomoListadoDTO {
  @IsNotEmpty()
  data: TomoDTO[];

  @IsNotEmpty()
  @IsNumber()
  count: number;

  @IsNotEmpty()
  @IsNumber()
  page: number;

  @IsNotEmpty()
  @IsNumber()
  pageSize: number;
}
