import { Test, TestingModule } from '@nestjs/testing';
import { TomoController } from './tomo.controller';

describe('TomoController', () => {
  let controller: TomoController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TomoController],
    }).compile();

    controller = module.get<TomoController>(TomoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
