import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { TomoService } from '../services/tomo.service';
import { TomoDTO, TomoUpdateDTO } from '../dtos/tomo.dto';
import { PaginationQueryDto } from 'src/core/shared/dtos';
import { AuthGuard } from 'src/core/guards/auth.guards';

@Controller('api/v1/tomo')
@ApiTags('PI Tomo')
@UseGuards(AuthGuard)
export class TomoController {
  constructor(private readonly tomoService: TomoService) {}

  @Post('add')
  @ApiOperation({ description: 'Crea un nuevo registro de Tomo.' })
  public async addTomo(@Body() body: TomoDTO) {
    return await this.tomoService.createTomo(body);
  }

  @Get('all')
  @ApiOperation({ description: 'Lista todos los Tomos.' })
  public async allTomo(@Query() pagination: PaginationQueryDto) {
    return await this.tomoService.findTomo(pagination);
  }

  @Get('combo/:id')
  @ApiOperation({
    description: 'Lista todos los Tomos pero con estado activo igual TRUE.',
  })
  public async comboTomo(@Param('id') id: number) {
    return await this.tomoService.findTomoCombo(id);
  }

  @Get('id/:id')
  @ApiOperation({
    description: 'Devuelve el Tomo segun el ID enviado.',
  })
  public async idTomo(@Param('id') id: number) {
    return await this.tomoService.findTomoById(id);
  }

  @Get('persona/:persona')
  @ApiOperation({
    description: 'Devuelve el Tomo segun el ID de la Persona.',
  })
  public async idTomoPersona(@Param('persona') persona: number) {
    return await this.tomoService.findTomoByIdByPersona(persona);
  }

  @Put('update/:id')
  @ApiOperation({ description: 'Actualiza los datos del Tomo.' })
  public async updateTomo(@Param('id') id: number, @Body() body: TomoDTO) {
    return await this.tomoService.updadeTomo(body, id);
  }

  @Put('updateFojas/:id')
  @ApiOperation({ description: 'Actualiza los datos de las fojas de un Tomo.' })
  public async updateFojas(
    @Param('id') id: number,
    @Body() body: TomoUpdateDTO,
  ) {
    console.log('$$$$$$$$$$$$$');
    console.log(body);
    console.log(id);
    console.log('$$$$$$$$$$$$$');

    return await this.tomoService.updateFojas(body, id);
  }
}
