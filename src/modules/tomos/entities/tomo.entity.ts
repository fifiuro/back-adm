import { BaseEntity } from 'src/core/shared/entities/base.entity';
import { CaratulaEntity } from 'src/modules/caratulas/entities/caratula.entity';
import { PersonasEntity } from 'src/modules/personas/entities/persona.entity';
import { PrestamoEntity } from 'src/modules/prestamos/entities/prestamo.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('tomo')
export class TomoEntity extends BaseEntity {
  @PrimaryGeneratedColumn({ name: 'id_tom' })
  idTom: number;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'tomo',
    unique: false,
  })
  tomo: string;

  @Column({
    type: 'int',
    nullable: false,
    name: 'fojas',
    unique: false,
  })
  fojas: number;

  @Column({
    type: 'int',
    nullable: false,
    name: 'cd',
    unique: false,
  })
  cd: number;

  @Column({
    type: 'int',
    nullable: false,
    name: 'rx',
    unique: false,
  })
  rx: number;

  @Column({
    type: 'int',
    nullable: false,
    name: 'tomografia',
    unique: false,
  })
  tomografia: number;

  @Column({
    type: 'varchar',
    nullable: false,
    name: 'estado',
    length: '50',
    unique: false,
  })
  estado: string;

  @Column({
    type: 'int',
    nullable: false,
    name: 'numero',
    unique: false,
  })
  numero: number;

  @ManyToOne(() => PersonasEntity, (persona: PersonasEntity) => persona.idPer)
  persona: PersonasEntity;

  @OneToMany(() => CaratulaEntity, (caratula: CaratulaEntity) => caratula.idCar)
  caratula: CaratulaEntity[];

  @OneToMany(() => PrestamoEntity, (prestamo: PrestamoEntity) => prestamo.idPre)
  prestamo: PrestamoEntity[];
}
