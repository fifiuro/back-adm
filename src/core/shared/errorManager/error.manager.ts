import { HttpException, HttpStatus } from '@nestjs/common';

export class ErrorManager extends Error {
  constructor({
    type,
    message,
  }: {
    type: keyof typeof HttpStatus;
    message: string;
  }) {
    super(`${type} :: ${message}`);
  }

  public static createSignatureError(message: string) {
    const name = message.split(' :: ')[0];
    const messageReady = message.split(' :: ')[1];
    if (name) {
      throw new HttpException(messageReady, HttpStatus[name]);
    } else {
      throw new HttpException(messageReady, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
