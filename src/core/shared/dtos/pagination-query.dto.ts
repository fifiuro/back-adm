import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsOptional, IsPositive, IsString } from 'class-validator';

export class PaginationQueryDto {
  @ApiProperty({ example: 10 })
  @Type(() => Number)
  @IsPositive()
  @IsOptional()
  limit: number;

  @ApiProperty({ example: 1 })
  @Type(() => Number)
  @IsPositive()
  @IsOptional()
  offset: number;
}

export class PaginationBuscarDto {
  @ApiProperty({ example: 'EFRAIN' })
  @IsOptional()
  @IsString()
  afiliado: string;

  @ApiProperty({ example: 'GERMAN' })
  @IsOptional()
  @IsString()
  medico: string;

  @ApiProperty({ example: 'PRESTAMO' })
  @IsOptional()
  @IsString()
  estado: string;

  @ApiProperty({ example: 10 })
  @Type(() => Number)
  @IsPositive()
  @IsOptional()
  limit: number;

  @ApiProperty({ example: 1 })
  @Type(() => Number)
  @IsPositive()
  @IsOptional()
  offset: number;
}
