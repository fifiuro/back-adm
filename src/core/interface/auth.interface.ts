export interface PayloadToken {
    sub: string;
    role: string;
}

export interface PayloadTokenExterno {
    usuario: string;
    contrasenia: string;
}

export interface AuthBody {
    username: string;
    password: string;
}

export interface AuthBodyExterno {
    idUsu: number;
    idRoles: number;
}

export interface AuthTokenResult {
    role: string;
    sub: string;
    iat: number;
    exp: number;
}

export interface IUseToken {
    role: string;
    sub: string;
    isExpired: boolean;
}