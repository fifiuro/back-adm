import { Module } from '@nestjs/common';
import { TipoAtencionModule } from './modules/tipo-atencion/tipo-atencion.module';
import { EspecialidadModule } from './modules/especialidad/especialidad.module';
import { SubirArchivosModule } from './modules/subir-archivos/subir-archivos.module';
import { PrestamosModule } from './modules/prestamos/prestamos.module';
import { AuthModule } from './modules/auth/auth.module';
import { ConfigModule } from '@nestjs/config';
import { DatabaseModule } from './core/database/database.module';
import { CarpetasModule } from './modules/carpetas/carpetas.module';
import { UnidadModule } from './modules/unidad/unidad.module';
import { PersonasModule } from './modules/personas/personas.module';
import { MedicosModule } from './modules/medicos/medicos.module';
import { CaratulasModule } from './modules/caratulas/caratulas.module';
import { TomosModule } from './modules/tomos/tomos.module';
import { PersonaUnidadModule } from './modules/persona-unidad/persona-unidad.module';
import { ReportesModule } from './modules/reportes/reportes.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    DatabaseModule,
    TipoAtencionModule,
    EspecialidadModule,
    SubirArchivosModule,
    PrestamosModule,
    AuthModule,
    CarpetasModule,
    UnidadModule,
    PersonasModule,
    MedicosModule,
    CaratulasModule,
    TomosModule,
    PersonaUnidadModule,
    ReportesModule,
  ],
  providers: [PersonasModule],
})
export class AppModule {}
